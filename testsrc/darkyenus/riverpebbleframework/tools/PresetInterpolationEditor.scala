package darkyenus.riverpebbleframework.tools

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{Color, Graphics, Graphics2D, RenderingHints}
import javax.swing._
import javax.swing.event.{ChangeEvent, ChangeListener}

import com.badlogic.gdx.math.Interpolation
import darkyenus.riverpebbleframework.util.PresetInterpolation
import net.miginfocom.swing.MigLayout

import scala.util.Try

/**
 * Private property.
 * User: Darkyen
 * Date: 08/05/14
 * Time: 15:00
 */
object PresetInterpolationEditor extends App {
  SwingUtilities.invokeLater(new Runnable {
    override def run(){
      val frame = new JFrame()
      frame.getContentPane.setLayout(new MigLayout("wrap 4","[grow,fill|grow,fill|grow,fill|grow,fill]","[grow,fill|grow,fill|]"))
      frame.setSize(400,400)


      val DefaultEditorText = "0.0f, 1.0f"
      val editorTextField = new JTextField(DefaultEditorText)
      val negativeNumbersCheckbox = new JCheckBox("Negative")

      val xLines = new JSpinner(new SpinnerNumberModel(10,0,100,1))
      val yLines = new JSpinner(new SpinnerNumberModel(10,0,100,1))

      def parseInputPoints:Seq[Float] = {
        editorTextField.getText.toLowerCase.replace('f',' ').split(',').map(v => Try(v.trim.toFloat)).filter(_.isSuccess).map(_.get)
      }

      val availableInterpolations = classOf[Interpolation].getFields.filter(field => field.getType.isAssignableFrom(classOf[Interpolation])).map(field => (field.getName,field.get(null).asInstanceOf[Interpolation])).toMap

      val interpolationSelector = new JComboBox(availableInterpolations.keys.toArray[String].sorted.asInstanceOf[Array[Object]])
      interpolationSelector.setSelectedItem("linear")

      val drawFrame = new JPanel(){
        override def paintComponent(g: Graphics){
          g match {
            case g2:Graphics2D =>
              g2.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON))
            case _ =>
          }
          val negative = negativeNumbersCheckbox.isSelected

          g.clearRect(0,0,getWidth,getHeight)
          val internalInterpolation = availableInterpolations(interpolationSelector.getSelectedItem.toString)
          val interpolation = new PresetInterpolation(parseInputPoints,internalInterpolation)
          g.setColor(Color.DARK_GRAY)
          g.fillRect(0,0,getWidth,getHeight)

          val precision = 1000
          val invPrecision = 1f/precision
          val width = (getWidth*0.8f).toInt
          val height = (getHeight*0.8f).toInt
          val x = (getWidth*0.1f).toInt
          val y = (getHeight*0.1f).toInt

          g.setColor(Color.BLACK)
          g.fillRect(x,y,width,height)

          g.setColor(Color.DARK_GRAY)
          val xLineCount = xLines.getValue.asInstanceOf[Int]
          val yLineCount = yLines.getValue.asInstanceOf[Int]

          for(lineX <- (1 until xLineCount+1).map(_ * (width.toFloat/(xLineCount+1)))){
            g.drawLine(x+lineX.round,y,x+lineX.round,y+height)
          }
          for(lineY <- (1 until yLineCount+1).map(_ * (height.toFloat/(yLineCount+1)))){
            g.drawLine(x,y+lineY.round,x+width,y+lineY.round)
          }

          g.setColor(Color.WHITE)
          for(seg <- 0 until (precision - 1)){
            val x1 = seg * invPrecision
            val x2 = x1 + invPrecision
            val y1 = interpolation(x1)
            val y2 = interpolation(x2)
            val yMul = if(negative)0.5f else 1f
            val yAdd = if(negative)-height/2 else 0
            g.drawLine(x+(x1 * width).round,y+height - (y1*height*yMul).round + yAdd,x+(x2 * width).round,y+height - (y2*height*yMul).round+yAdd)
          }
        }
      }

      editorTextField.addActionListener(new ActionListener{
        override def actionPerformed(p1: ActionEvent){
          val inputPoints = parseInputPoints
          if(inputPoints.size < 1){
            editorTextField.setText(DefaultEditorText)
          }else{
            editorTextField.setText(inputPoints.map(n => s"${n}f").addString(new StringBuilder,", ").toString())
          }
          drawFrame.repaint()
        }
      })

      val repaintActionListener = new ActionListener {
        override def actionPerformed(p1: ActionEvent){
          drawFrame.repaint()
        }
      }
      val repaintChangeListener = new ChangeListener{
        override def stateChanged(p1: ChangeEvent){
          drawFrame.repaint()
        }
      }
      interpolationSelector.addActionListener(repaintActionListener)
      negativeNumbersCheckbox.addActionListener(repaintActionListener)
      xLines.addChangeListener(repaintChangeListener)
      yLines.addChangeListener(repaintChangeListener)


      frame.getContentPane.add(drawFrame,"span 4 2")
      frame.getContentPane.add(editorTextField,"span 3,grow")
      frame.getContentPane.add(interpolationSelector)
      frame.getContentPane.add(negativeNumbersCheckbox)
      frame.getContentPane.add(xLines)
      frame.getContentPane.add(yLines)
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
      frame.setVisible(true)
    }
  })
}
