package darkyenus.riverpebbleframework.tests

import darkyenus.riverpebbleframework.{Render2DContext, Render2D}
import darkyenus.riverpebbleframework.flowui._
import com.badlogic.gdx.{ApplicationListener, Gdx}
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.graphics.{Texture, Color}

/**
 * Private property.
 * User: Darkyen
 * Date: 15/04/14
 * Time: 12:34
 */
object UITestScrollContainer extends ApplicationListener with App {
  var render2D:Render2D = _
  var elementRoot:UIRoot = _
  var scrollContainer:UIScrollableContainer = _
  var innerContainer:UIMinimalSizeElement = _

  override def dispose(): Unit = {}

  override def resume(): Unit = {}

  override def pause(): Unit = {}

  override def render(): Unit = {
    render2D(Gdx.graphics.getWidth)(context => {
      elementRoot.process(context)
    })
  }

  override def resize(width: Int, height: Int): Unit = {}

  override def create(): Unit = {
    render2D = new Render2D()
    Render2D.DefaultFont.setUseIntegerPositions(false)
    Render2D.DefaultFont.getRegion(0).getTexture.setFilter(Texture.TextureFilter.Linear,Texture.TextureFilter.Linear)
    Render2D.DefaultFont.setScale(0.05f/Render2D.DefaultFont.getLineHeight)
    innerContainer = new UIMinimalSizeElement {
      override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
        context.spriteBatch.setColor(Color.RED)
        context.spriteBatch.draw(Render2D.White,x,y,width,height)
        context.spriteBatch.setColor(Color.BLUE)
        val off = 0.01f
        context.spriteBatch.draw(Render2D.White,x+off,y+off,width-2*off,height-2*off)

        Render2D.DefaultFont.setColor(Color.GREEN)
        context.drawCentered(Render2D.DefaultFont,s"${scrollContainer.scrollXFraction}x ${scrollContainer.scrollYFraction}y $width $height",x+width/2,y+height/2)
        processEvents({
          case TapEvent(tapX,tapY,pointer,button,duration) =>
            if(button == 1){
              scrollContainer.scrollXFraction += duration
              scrollContainer.scrollYFraction += duration
            }else{
              scrollContainer.scrollXFraction -= duration
              scrollContainer.scrollYFraction -= duration
            }
        })
      }

      override def minimalWidth(width:Float,height:Float): Option[Float] = Some(1f)

      override def minimalHeight(width:Float,height:Float): Option[Float] = Some(1f)
    }
    scrollContainer = new UIScrollableContainer(innerContainer)
    elementRoot = new UIRoot(new UIFlowContainer(Orientation.Horizontal,Seq(scrollContainer)))
    Gdx.input.setInputProcessor(elementRoot)
  }

  new LwjglApplication(this)
}
