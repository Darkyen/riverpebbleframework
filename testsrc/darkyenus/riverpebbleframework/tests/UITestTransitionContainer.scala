package darkyenus.riverpebbleframework.tests

import com.badlogic.gdx.{Gdx, ApplicationListener}
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import darkyenus.riverpebbleframework.{Render2DContext, Render2D}
import darkyenus.riverpebbleframework.flowui._
import com.badlogic.gdx.math.{Vector2, MathUtils}
import com.badlogic.gdx.utils.TimeUtils
import com.badlogic.gdx.graphics.Color
import darkyenus.riverpebbleframework.flowui.transitionoperators.{MoveOutOperator, MoveInOperator}
import scala.util.Random

/**
 * Private property.
 * User: Darkyen
 * Date: 11/04/14
 * Time: 22:56
 */
object UITestTransitionContainer extends ApplicationListener with App {

  var render2D:Render2D = _
  var elementRoot:UIRoot = _
  var elementStructure:UITransitionContainer = _
  var blueElement:UIElement = _
  var redElement:UIElement = _

  override def dispose(): Unit = {}

  override def resume(): Unit = {}

  override def pause(): Unit = {}

  override def render(): Unit = {
    render2D(Gdx.graphics.getWidth)(context => {
      elementRoot.process(context)
    })
  }

  override def resize(width: Int, height: Int): Unit = {}

  override def create(): Unit = {
    render2D = new Render2D()
    blueElement = new UIElement {
      override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
        context.spriteBatch.setColor(Color.BLUE)
        context.spriteBatch.draw(Render2D.White,x,y,width,height)
        processEvents({
          case TapEvent(tapX,tapY,pointer,button,duration) =>
            elementStructure.transitionTo(redElement,1f,new MoveInOperator(new Vector2(1f,0f)))
        })
      }
    }
    redElement = new UIElement {
      override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
        context.spriteBatch.setColor(Color.RED)
        context.spriteBatch.draw(Render2D.White,x,y,width,height)
        processEvents({
          case TapEvent(tapX,tapY,pointer,button,duration) =>
            elementStructure.transitionTo(blueElement,5f,new MoveOutOperator(new Vector2(-1f,-1f)))
        })
      }
    }
    elementStructure = new UITransitionContainer(blueElement)
    val filler = new UIElement {
      override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
        context.spriteBatch.setColor(Random.nextFloat())
        context.spriteBatch.draw(Render2D.White,x,y,width,height)
      }
    }
    elementRoot = new UIRoot(new UIFlowContainer(Orientation.Horizontal,Seq(filler,elementStructure,filler)))
    Gdx.input.setInputProcessor(elementRoot)
  }

  new LwjglApplication(this)
}
