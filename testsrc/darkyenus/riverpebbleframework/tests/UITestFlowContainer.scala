package darkyenus.riverpebbleframework.tests

import com.badlogic.gdx.{Gdx, ApplicationListener}
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import darkyenus.riverpebbleframework.{Render2DContext, Render2D}
import darkyenus.riverpebbleframework.flowui._
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.utils.TimeUtils
import com.badlogic.gdx.graphics.Color

/**
 * Private property.
 * User: Darkyen
 * Date: 11/04/14
 * Time: 22:56
 */
object UITestFlowContainer extends ApplicationListener with App {

  var render2D:Render2D = _
  var elementStructure:UIElement = _

  override def dispose(): Unit = {}

  override def resume(): Unit = {}

  override def pause(): Unit = {}

  override def render(): Unit = {
    render2D(Gdx.graphics.getWidth)(context => {
      elementStructure.process(0f,0f,0f,1f,Gdx.graphics.getHeight.toFloat/Gdx.graphics.getWidth,context)(null)
    })
  }

  override def resize(width: Int, height: Int): Unit = {}

  override def create(): Unit = {
    render2D = new Render2D()
    elementStructure = new UIFlowContainer(Orientation.Horizontal,Seq(
    new UIFlowContainer(Orientation.Vertical,Seq(
            new UIElement {
              //override def width: Size = Absolute(MathUtils.sin(TimeUtils.millis()/1000f))

              override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
                context.spriteBatch.setColor(Color.RED)
                context.spriteBatch.draw(Render2D.White,x,y,width,height)
                context.spriteBatch.setColor(Color.BLUE)
                val off = 0.01f
                context.spriteBatch.draw(Render2D.White,x+off,y+off,width-2*off,height-2*off)
              }
            },
          new UIElement {
            override def height: Size = Absolute(MathUtils.sinDeg((TimeUtils.millis()+90)/10 % 180)*0.6f)

            override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
              context.spriteBatch.setColor(Color.YELLOW)
              context.spriteBatch.draw(Render2D.White,x,y,width,height)
              context.spriteBatch.setColor(Color.BLUE)
              val off = 0.01f
              context.spriteBatch.draw(Render2D.White,x+off,y+off,width-2*off,height-2*off)
            }
          },
          new UIElement {
            //override def width: Size = Absolute(MathUtils.sin(TimeUtils.millis()/1000f))

            override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
              context.spriteBatch.setColor(Color.RED)
              context.spriteBatch.draw(Render2D.White,x,y,width,height)
              context.spriteBatch.setColor(Color.BLUE)
              val off = 0.01f
              context.spriteBatch.draw(Render2D.White,x+off,y+off,width-2*off,height-2*off)
            }
          }
    )),
    new UIElement {
      override def width: Size = Absolute(MathUtils.sinDeg(TimeUtils.millis()/10 % 180)*0.6f)

      override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
        context.spriteBatch.setColor(Color.RED)
        context.spriteBatch.draw(Render2D.White,x,y,width,height)
        context.spriteBatch.setColor(Color.BLUE)
        val off = 0.01f
        context.spriteBatch.draw(Render2D.White,x+off,y+off,width-2*off,height-2*off)
      }
    }
    ))
  }

  new LwjglApplication(this)
}
