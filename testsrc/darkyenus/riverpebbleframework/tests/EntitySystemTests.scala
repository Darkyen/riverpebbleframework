package darkyenus.riverpebbleframework.tests

import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.junit.Test
import darkyenus.riverpebbleframework.entity._
import darkyenus.riverpebbleframework.data.Data

/**
 * Private property.
 * User: Darkyen
 * Date: 12/02/14
 * Time: 21:26
 */
@RunWith(classOf[JUnit4])
class EntitySystemTests {

  @Test
  def uniqueComponentIDs(){
    val manager = new EntityManager()
    assert(manager.getComponentID(new TestComponent1) == 0,"Was: "+manager.getComponentID(new TestComponent1))
    assert(manager.getComponentID(new TestComponent2) == 1,"Was: "+manager.getComponentID(new TestComponent2))
    assert(manager.getComponentID(new TestComponent1) == 0,"Was: "+manager.getComponentID(new TestComponent1))
    assert(manager.getComponentID(new TestComponent2) == 1,"Was: "+manager.getComponentID(new TestComponent2))
    val manager2 = new EntityManager()
    assert(manager2.getComponentID(new TestComponent2) == 0,"Was: "+manager2.getComponentID(new TestComponent2))
    assert(manager2.getComponentID(new TestComponent1) == 1,"Was: "+manager2.getComponentID(new TestComponent1))
    assert(manager2.getComponentID(new TestComponent1) == 1,"Was: "+manager2.getComponentID(new TestComponent1))
    assert(manager2.getComponentID(new TestComponent2) == 0,"Was: "+manager2.getComponentID(new TestComponent2))
  }

  @Test
  def addEntities(){
    val manager = new EntityManager()
    val entities = 1000
    for(i <- 0 until entities){
      assert(manager.addEntity(new TestComponent1,new TestComponent2) == new Entity(i))
      assert(manager.hasEntity(new Entity(i)))
      assert(manager.hasEntityComponent(new Entity(i),classOf[TestComponent1]))
      assert(manager.hasEntityComponent(new Entity(i),classOf[TestComponent2]))
      assert(manager.getEntity(new Entity(i)).size == 2,"Not added properly, size on run "+i+" was : "+manager.getEntity(new Entity(i)).size)
    }
    assert(!manager.hasEntity(new Entity(entities)))
    assert(!manager.hasEntityComponent(new Entity(entities),classOf[TestComponent1]))
    assert(!manager.hasEntityComponent(new Entity(entities),classOf[TestComponent2]))

    assert(manager.getEntity(new Entity(entities)).size == 0)
    assert(manager.getEntityCount == entities)
  }

  @Test
  def removeEntities(){
    val manager = new EntityManager()
    assert(manager.addEntity(new TestComponent1) == new Entity(0))
    assert(manager.addEntity(new TestComponent1) == new Entity(1))
    assert(manager.addEntity(new TestComponent2) == new Entity(2))
    assert(manager.addEntity(new TestComponent1) == new Entity(3))
    assert(manager.addEntity(new TestComponent1) == new Entity(4))
    assert(manager.getEntityCount == 5)
    assert(manager.removeEntity(new Entity(2)).exists(_.isInstanceOf[TestComponent2]))
    assert(!manager.hasEntity(new Entity(2)))

    assert(manager.getReusableIndexesCount == 1)
    assert(manager.hasEntity(new Entity(3)))
    manager.removeEntity(new Entity(3))
    assert(!manager.hasEntity(new Entity(3)))
    assert(manager.getReusableIndexesCount == 2)
    assert(manager.hasEntity(new Entity(0)))
    manager.removeEntity(new Entity(0))
    assert(!manager.hasEntity(new Entity(0)))
    assert(manager.getReusableIndexesCount == 3)
    assert(manager.hasEntity(new Entity(4)))
    manager.removeEntity(new Entity(4))
    assert(!manager.hasEntity(new Entity(4)))
    assert(manager.getReusableIndexesCount == 1)

    manager.removeEntity(new Entity(1))
    assert(manager.getReusableIndexesCount == 0)
    assert(manager.getEntityCount == 0)
  }

  @Test
  def entityComponentRetrievalTest(){
    val manager = new EntityManager()
    manager.addEntity(new TestComponent1)
    assert(manager.getEntityComponent(new Entity(0),classOf[TestComponent1]).data1() == "Test1")
    manager.getEntityComponent(new Entity(0),classOf[TestComponent1]).data1() = "TEST1"
    //assert(manager.getEntityComponent[TestComponent1](new Entity(0)).data() == "Test1")
    //manager.getEntityComponent[TestComponent1](new Entity(0)).data.push(1)
    assert(manager.getEntityComponent(new Entity(0),classOf[TestComponent1]).data1() == "TEST1")
  }

  @Test(expected = classOf[NoSuchElementException])
  def entityComponentRetrievalTestFail(){
    val manager = new EntityManager()
    manager.addEntity(new TestComponent1)
    manager.getEntityComponent[TestComponent1](new Entity(0))
    manager.getEntityComponent[TestComponent2](new Entity(0))
  }

  @Test
  def entitySystemBasicTest(){
    val info = "356345"
    val manager = new EntityManager(Array[EntitySystem](new EntitySystem{

      override def process(delta: Float, timestamp: Long, entities: Iterable[Entity])(implicit manager: EntityManagerDelegate){
        for(entity <- entities){
          entity.getComponent[TestComponent1].data1() = info
        }
      }

      override val requiredComponents: Iterable[Class[_ <: Component]] = Iterable(classOf[TestComponent1])
    },new EntitySystem{

      override def process(delta: Float, timestamp: Long, entities: Iterable[Entity])(implicit manager: EntityManagerDelegate){
        for(entity <- entities){
          assert(entity.getComponent[TestComponent1].data1() == info)
        }
      }

      override val requiredComponents: Iterable[Class[_ <: Component]] = Iterable(classOf[TestComponent1])
    }))
    (0 until 10).foreach(i => manager.addEntity(new TestComponent1))
    (0 until 5).foreach(i => manager.removeEntity(new Entity(i*2)))
    (0 until 10).foreach(i => manager.addEntity(new TestComponent2))
    (0 until 5).foreach(i => manager.removeEntity(new Entity(i*2)))
    (0 until 10).foreach(i => manager.addEntity(new TestComponent1))
    manager.process(1)
  }

  @Test
  def componentDataTest(){
    assert(new TestComponent1().components.size == 1)
    assert(new TestComponent2().components.size == 2)
    assert(new TestComponent3().components.size == 2)
  }

  @Test
  def complexComponentTest(){
    val manager = new EntityManager()
    val entity1 = manager.addEntity(new TestComponent1,new TestComponent3)
    assert(manager.hasEntity(entity1))
    assert(manager.hasEntityComponent(entity1,classOf[TestComponent1]))
    assert(!manager.hasEntityComponent(entity1,classOf[TestComponent2]))
    assert(manager.hasEntityComponent(entity1,classOf[TestComponent3]))
  }

  class TestComponent1 extends Component {
    val data1:Data[String] = "Test1"
  }

  class TestComponent2 extends Component {
    val data1:Data[String] = "Test2"
    val data2:Data[String] = "Test2"
  }

  class TestComponent3 extends Component {
    val data1:Data[String] = "Test"
    val countdownData:CountdownData = 5f
  }
}
