package darkyenus.riverpebbleframework.tests

import com.badlogic.gdx._
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.graphics.g2d.BitmapFont
import darkyenus.riverpebbleframework.Render2D
import org.lwjgl.opengl.Display

/**
 * Private property.
 * User: Darkyen
 * Date: 06/03/14
 * Time: 22:48
 */
object MouseGrabTest extends App {
  val app = new LwjglApplication(new ApplicationListener {

    var font:BitmapFont = null
    var render2d:Render2D = null

    override def create(){
      println("Create")
      Display.setTitle(Gdx.graphics.getWidth+" "+Gdx.graphics.getHeight)
      font = new BitmapFont(true)
      render2d = new Render2D()
      Gdx.input.setInputProcessor(new InputAdapter{
        override def mouseMoved(screenX: Int, screenY: Int): Boolean = {
          println("Mm("+screenX+" "+screenY+")")
          true
        }

        override def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = {
          println("Td("+screenX+" "+screenY+")")
          true
        }

        override def keyUp(keycode: Int): Boolean = {
          if(keycode == Input.Keys.SPACE){
            Gdx.input.setCursorCatched(false)
          }
          true
        }

        override def keyDown(keycode: Int): Boolean = {
          if(keycode == Input.Keys.SPACE){
            Gdx.input.setCursorCatched(true)
          }
          true
        }
      })
      //Gdx.input.setCursorCatched(false)
    }

    override def resume(){
      println("Resume")
    }

    override def render(){
      Gdx.input.setCursorCatched(Gdx.input.isKeyPressed(Input.Keys.SPACE))
      Render2D.clearScreen()
      render2d()(context => {
        font.draw(context,Gdx.input.getX+" "+Gdx.input.getY+" "+Gdx.input.getDeltaX+" "+Gdx.input.getDeltaY,50,50)
        context.draw(Render2D.White,Gdx.input.getX-1,Gdx.input.getY-1,1,1)
      })

    }

    override def pause(){}

    override def dispose(){}

    override def resize(width: Int, height: Int){}
  })
}
