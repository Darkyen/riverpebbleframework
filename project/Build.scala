
import sbt.Keys._
import sbt._

object Build extends Build {
	val gdxVersion = "1.5.3"

	val SharedSettings = Seq(
		organization := "darkyenus",
		scalaVersion := "2.11.5",
		javaOptions += "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005",
		fork in run := true,
		exportJars := true,
		startYear := Some(2014),
		resolvers += "Maven Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
		scalacOptions ++= Seq("-deprecation","-feature","-target:jvm-1.6"),
		javacOptions ++= Seq("-source","6","-target","6")
	)

	lazy val RiverpebbleFramework: Project = Project("RiverpebbleFramework", file("."), settings = SharedSettings ++ Seq(
		name := "RiverpebbleFramework",
		version := "1.0-SNAPSHOT",
		scalaSource in Compile := baseDirectory.value / "src",
		scalaSource in Test := baseDirectory.value / "testsrc",
		libraryDependencies ++= Seq(
			"com.badlogicgames.gdx" % "gdx" % gdxVersion,
			"com.esotericsoftware" % "kryonet" % "2.22.0-RC1",

			"com.novocode" % "junit-interface" % "0.8" % "test->default",
			"com.badlogicgames.gdx" % "gdx-backend-lwjgl" % gdxVersion % "test",
			"com.badlogicgames.gdx" % "gdx-platform" % gdxVersion % "test" classifier "natives-desktop",
			"com.miglayout" % "miglayout-swing" % "4.2" % "test"
		),
		scalacOptions ++= Seq("-Ybackend:GenBCode")
	))

	lazy val RiverpebbleFrameworkTools: Project = Project("RiverpebbleFrameworkTools", file("RiverpebbleFrameworkTools"), settings = SharedSettings ++ Seq(
		name := "RiverpebbleFrameworkTools",
		version := "1.0-SNAPSHOT",
		crossScalaVersions in Compile := Seq(scalaVersion.value, "2.10.4"),
		scalaSource in Compile := baseDirectory.value / "src",
		resourceDirectory in Compile := baseDirectory.value / "resources",
		libraryDependencies ++= Seq(
			"com.badlogicgames.gdx" % "gdx" % gdxVersion,
			"com.badlogicgames.gdx" % "gdx-backend-lwjgl" % gdxVersion,
			"com.badlogicgames.gdx" % "gdx-platform" % gdxVersion classifier "natives-desktop",
			"com.badlogicgames.gdx" % "gdx-tools" % gdxVersion,
			"com.google.guava" % "guava" % "17.0",
			"org.apache.xmlgraphics" % "batik-transcoder" % "1.7",
			"org.apache.xmlgraphics" % "batik-codec" % "1.7"
		)
	))
}