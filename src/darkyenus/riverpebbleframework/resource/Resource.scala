package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.FileHandleResolver
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.utils.GdxRuntimeException

/**
 * Private property.
 * User: Darkyen
 * Date: 22/12/13
 * Time: 19:15
 */
trait Resource [ResourceType] {
  protected def getResource:ResourceType

  private final var cache:ResourceType = _

  final def apply():ResourceType = {
    if(cache == null){
	    try {
        cache = getResource
	    }catch {
		    case t:GdxRuntimeException =>
			    Gdx.app.error("Resource","Couldn't load "+this.toString)
			    throw t
	    }
    }
    cache
  }
}

object Resource extends AssetManagerHolder {

	private final var assetManager:AssetManager = null

	final def initialize(resolver:FileHandleResolver): Unit ={
		assetManager = new AssetManager(resolver)
	}

	final def initialize(): Unit ={
		assetManager = new AssetManager()
	}

	@inline
  def AssetManager:AssetManager = assetManager
}

/**
 * A cool trick, thanks to this and its companion object, you can use Resource object as a AssetManager instance!
 * It's like magic! (Only cooler)
 */
trait AssetManagerHolder {
  def AssetManager:AssetManager
}

object AssetManagerHolder {
  import scala.language.implicitConversions

	@inline
  implicit def getAssetManager(holder:AssetManagerHolder):AssetManager = holder.AssetManager
}
