package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import darkyenus.riverpebbleframework.resource.TextureAtlasResource.TextureAtlasResource
import com.badlogic.gdx.assets.loaders.SkinLoader.SkinParameter

/**
 * Private property.
 * User: Darkyen
 * Date: 18/05/14
 * Time: 21:48
 */
object SkinResource {

  def from(file:String, textureAtlas:TextureAtlasResource):SkinResource = {
    lazy val fileHandle = Gdx.files.internal(file)
    lazy val assetDescriptor = new AssetDescriptor[Skin](fileHandle,classOf[Skin])
    lazy val skin = new Skin(fileHandle,textureAtlas())
    new SkinResource(skin,assetDescriptor)
  }

  /**
   * .json is appended to the file and then passed as internal handle to #from
   * @see SkinResource#from(FileHandle,SkinParameter)
   */
  def from(file:String):SkinResource = {
    from(Gdx.files.internal(file+".json"),null)
  }

  def from(fileHandle:FileHandle, parameter:SkinParameter):SkinResource = {
    val assetDescriptor = new AssetDescriptor[Skin](fileHandle,classOf[Skin],parameter)
    Resource.load(assetDescriptor)

    new SkinResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[Skin])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[Skin](assetDescriptor)
    },assetDescriptor)
  }

  class SkinResource(skin: => Skin, descriptor:AssetDescriptor[Skin]) extends Resource[Skin] {
    def getResource: Skin = skin

    def assetDescriptor: AssetDescriptor[Skin] = descriptor

	  override def toString: String = {
		  "SkinResource "+descriptor.file.file().getCanonicalPath+" "+descriptor.params
	  }
  }
}
