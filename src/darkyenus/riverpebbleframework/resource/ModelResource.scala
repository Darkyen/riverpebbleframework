package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.loaders.ModelLoader
import com.badlogic.gdx.assets.loaders.ModelLoader.ModelParameters
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.g3d.{ModelInstance, Model}

/**
 * Private property.
 * User: Darkyen
 * Date: 24/07/14
 * Time: 16:29
 */
object ModelResource {
  def from(fileHandle:FileHandle, parameterModifier:(ModelLoader.ModelParameters)=>Unit = (p)=>{}):ModelResource = {
    val assetParameters = new ModelParameters
    parameterModifier(assetParameters)
    val assetDescriptor = new AssetDescriptor[Model](fileHandle,classOf[Model],assetParameters)
    Resource.load(assetDescriptor)
    new ModelResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[Model])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[Model](assetDescriptor)
    },assetDescriptor)
  }

  class ModelResource(bitmapFont: => Model,descriptor:AssetDescriptor[Model]) extends Resource[Model] {
    def getResource: Model = bitmapFont

    def assetDescriptor: AssetDescriptor[Model] = descriptor

    def createInstance(node:String):ModelInstance = {
      new ModelInstance(apply(),node)
    }

    def createInstance():ModelInstance = {
      new ModelInstance(apply())
    }

	  override def toString: String = {
		  "ModelResource "+descriptor.file.file().getCanonicalPath+" "+descriptor.params
	  }
  }
}
