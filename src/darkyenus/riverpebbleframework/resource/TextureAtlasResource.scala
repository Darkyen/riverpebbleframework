package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader.TextureAtlasParameter
import com.badlogic.gdx.Gdx

/**
 * Private property.
 * User: Darkyen
 * Date: 14/01/14
 * Time: 19:39
 */
object TextureAtlasResource {

  /**
   * Same as the other from, but string is automatically resolved to Internal file handle which you should be using anyway.
   * Also, you can't do anything else than flip right now, so why make it difficult to do so.
   * This framework uses flipped Y axis, so it is default
   */
  def from(file:String, flip:Boolean = true):TextureAtlasResource = {
    from(Gdx.files.internal(file), p => p.flip = flip)
  }

  def from(file:FileHandle, flip:Boolean):TextureAtlasResource = {
    from(file, p => p.flip = flip)
  }

  def from(fileHandle:FileHandle, parameterModifier:(TextureAtlasLoader.TextureAtlasParameter)=>Unit):TextureAtlasResource = {
    val assetParameters = new TextureAtlasParameter
    parameterModifier(assetParameters)
    val assetDescriptor = new AssetDescriptor[TextureAtlas](fileHandle,classOf[TextureAtlas],assetParameters)
    Resource.load(assetDescriptor)
    new TextureAtlasResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[TextureAtlas])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[TextureAtlas](assetDescriptor)
    },assetDescriptor)
  }

  class TextureAtlasResource(texture: => TextureAtlas, descriptor:AssetDescriptor[TextureAtlas]) extends Resource[TextureAtlas] {
    def getResource: TextureAtlas = texture

    def assetDescriptor: AssetDescriptor[TextureAtlas] = descriptor

	  override def toString: String = {
		  "TextureAtlasResource "+descriptor.file.file().getCanonicalPath+" "+descriptor.params
	  }
  }

}
