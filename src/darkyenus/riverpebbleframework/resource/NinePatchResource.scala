package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.graphics.g2d.NinePatch

/**
 * Private property.
 * User: Darkyen
 * Date: 16/04/14
 * Time: 11:23
 */
object NinePatchResource {

  def from(texture:TextureAtlasResource.TextureAtlasResource,name:String):NinePatchResource = {
    new NinePatchResource(texture().createPatch(name))
  }

  class NinePatchResource(ninePatch: => NinePatch) extends Resource[NinePatch] {
    def getResource: NinePatch = ninePatch

	  override def toString: String = {
		  "NinePatchResource "+ninePatch.toString
	  }
  }
}
