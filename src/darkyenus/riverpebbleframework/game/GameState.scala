package darkyenus.riverpebbleframework.game

import com.badlogic.gdx.InputMultiplexer

/**
 * Private property.
 * User: Darkyen
 * Date: 21/12/13
 * Time: 16:03
 */
abstract class GameState extends InputMultiplexer {

	private var initialized = false

	private[riverpebbleframework] final def processGameState(delta:Float): GameState ={
		process(delta)
		changeState
	}

  private var changeState:GameState = this

  final def next(state:GameState){
    changeState = state
  }

  def process(delta:Float)

  def initialize(){}

  def show(){}

  def hide(){}

  def resize(width: Int, height: Int){}

  def pause(){}

  def resume(){}

	@inline
	private[riverpebbleframework] def makeSureInitialized(): Unit ={
		if(!initialized){
			initialize()
			initialized = true
		}
	}
}
