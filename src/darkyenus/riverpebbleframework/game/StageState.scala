package darkyenus.riverpebbleframework.game

import com.badlogic.gdx.graphics.g2d.{Batch, SpriteBatch}
import com.badlogic.gdx.utils.viewport.{ScreenViewport, Viewport}
import com.badlogic.gdx.scenes.scene2d.Stage
import darkyenus.riverpebbleframework.Render2D

/**
 * Private property.
 * User: Darkyen
 * Date: 03/07/14
 * Time: 14:07
 */
class StageState(val batch:Batch = new SpriteBatch(), val viewport:Viewport = new ScreenViewport()) extends GameState {
	private final val stageInst = new Stage(viewport,batch)
	stageInst.getRoot.setTransform(false)
  addProcessor(stageInst)

	@inline
	final def stage:Stage = stageInst

  override def process(delta: Float){
    Render2D.clearScreen()
    stageInst.act(delta)
    stageInst.draw()
  }

  override def resize(width: Int, height: Int){
    stageInst.getViewport.update(width, height, true)
  }
}
