package darkyenus.riverpebbleframework.game

import com.badlogic.gdx.{Gdx, InputProcessor, ApplicationListener}
import darkyenus.riverpebbleframework.Render2D

/**
 * Private property.
 * User: Darkyen
 * Date: 20/12/13
 * Time: 21:09
 */
abstract class StatefulGame () extends ApplicationListener with InputProcessor {

  private var state:GameState = null

  /**
   * To be overridden.
   * Initialize your game here.
   */
  def initialize():GameState

  override def dispose(){
    state.hide()
  }

  override def resume(){
    state.resume()
  }

  override def pause(){
    state.pause()
  }

  override def render(){
    val newState = state.processGameState(Gdx.graphics.getDeltaTime)
    if(newState != null){
      state.hide()
      state.next(null)
      newState.makeSureInitialized()
      newState.show()
      newState.resize(Gdx.graphics.getWidth,Gdx.graphics.getHeight)
      state = newState
    }
    Render2D.resized = false
  }

  override def resize(width: Int, height: Int){
    state.resize(width,height)
    Render2D.resized = true
  }

  override def create(){
    Gdx.input.setInputProcessor(this)
    state = initialize()
    state.makeSureInitialized()
    state.show()
  }

  override final def scrolled(amount: Int): Boolean = {
    state.scrolled(amount)
    true
  }

  override final def mouseMoved(screenX: Int, screenY: Int): Boolean = {
    state.mouseMoved(screenX,screenY)
    true
  }

  override final def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = {
    state.touchDragged(screenX,screenY,pointer)
    true
  }

  override final def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
    state.touchUp(screenX,screenY,pointer,button)
    true
  }

  override final def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
    state.touchDown(screenX,screenY,pointer,button)
    true
  }

  override final def keyTyped(character: Char): Boolean = {
    state.keyTyped(character)
    true
  }

  override final def keyUp(keyCode: Int): Boolean = {
    state.keyUp(keyCode)
    true
  }

  override final def keyDown(keyCode: Int): Boolean = {
    state.keyDown(keyCode)
    true
  }
}
