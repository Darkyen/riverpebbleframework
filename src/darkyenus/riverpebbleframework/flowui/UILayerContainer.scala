package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 13/04/14
 * Time: 14:55
 */
class UILayerContainer (layers:Seq[UIElement]) extends UIElement {
  override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
    for(layer <- layers){
      layer.process(delta,x,y,width,height,context)(events)
    }
  }
}
