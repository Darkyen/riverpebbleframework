package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 17/04/14
 * Time: 09:27
 */
abstract class UIButton extends UIElement {

  def renderButton(x: Float, y: Float, width: Float, height: Float, context: Render2DContext)

  def onTap(duration:Float)

  override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
    processEvents({
      case tap:TapEvent if tap.isContainedIn(x,y,width,height) =>
        onTap(tap.duration)
    })
    renderButton(x,y,width,height,context)
  }
}
