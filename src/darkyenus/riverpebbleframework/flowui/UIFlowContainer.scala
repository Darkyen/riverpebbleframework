package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext
import darkyenus.riverpebbleframework.flowui.Orientation.Orientation

/**
 * Private property.
 * User: Darkyen
 * Date: 11/04/14
 * Time: 22:00
 */
class UIFlowContainer(
                   flowOrientation:Orientation,
                   elements:Seq[UIElement]) extends UIElement {

  override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
    var fillingElementCount = 0
    var spaceForFilling:Float = flowOrientation match {
      case Orientation.Horizontal => width
      case Orientation.Vertical => height
    }
    for(element <- elements){
      (flowOrientation match {
        case Orientation.Horizontal =>
          element.width
        case Orientation.Vertical =>
          element.height
      }) match {
        case Fill =>
          fillingElementCount += 1
        case Absolute(size) =>
          spaceForFilling -= size
        case fraction:Fraction =>
          spaceForFilling -= fraction(width,height)
      }
    }
    lazy val sizeOfFillingElement = spaceForFilling/fillingElementCount //Lazy, because divide by zero. Will get retrieved only when any filling exists

    var elementX = x
    var elementY = y
    for(element <- elements){
      val elementWidth = flowOrientation match {
        case Orientation.Horizontal => element.width match {
          case Fill => sizeOfFillingElement
          case Absolute(size) => size
          case fraction:Fraction => fraction(width,height)
        }
        case Orientation.Vertical => width
      }
      val elementHeight = flowOrientation match {
        case Orientation.Horizontal => height
        case Orientation.Vertical => element.height match {
          case Fill => sizeOfFillingElement
          case Absolute(size) => size
          case fraction:Fraction => fraction(width,height)
        }
      }

      element.process(delta,elementX,elementY,elementWidth,elementHeight,context)

      flowOrientation match {
        case Orientation.Horizontal =>
          elementX += elementWidth
        case Orientation.Vertical =>
          elementY += elementHeight
      }
    }
  }
}
