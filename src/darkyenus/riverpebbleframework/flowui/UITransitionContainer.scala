package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext
import darkyenus.riverpebbleframework.util.SlowValue

/**
 * Private property.
 * User: Darkyen
 * Date: 13/04/14
 * Time: 15:35
 */
class UITransitionContainer(private var element:UIElement) extends UIElement {

  private var transitioningTo:UIElement = null
  private val transitionProgress:SlowValue = new SlowValue(1f,1f,1f,() => {
    element = transitioningTo
    transitioningTo = null
  })
  private var transitionOperator:TransitionOperator = null

  override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]): Unit = {
    transitionProgress.process(delta)
    if(transitionProgress.stable){
      element.process(delta,x,y,width,height,context)(events)
    }else{
      transitionOperator.drawMidTransition(transitionProgress(),element,transitioningTo,x,y,width,height,context)
    }
  }

  def transitionTo(element:UIElement,duration:Float,operator:TransitionOperator){
    transitioningTo = element
    transitionOperator = operator
    transitionProgress.value = 0f
    transitionProgress.speed = 1f/duration
  }
}

trait TransitionOperator {
  def drawMidTransition(progress:Float, from:UIElement, to:UIElement, x:Float, y:Float, width:Float, height:Float, context:Render2DContext)
}
