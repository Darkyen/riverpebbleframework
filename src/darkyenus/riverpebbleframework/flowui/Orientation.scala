package darkyenus.riverpebbleframework.flowui

/**
 * Private property.
 * User: Darkyen
 * Date: 11/04/14
 * Time: 22:01
 */
object Orientation extends Enumeration {
  type Orientation = Value
  val Horizontal, Vertical = Value
}
