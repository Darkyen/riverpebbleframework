package darkyenus.riverpebbleframework.flowui.transitionoperators

import com.badlogic.gdx.math.{Rectangle, Vector2}
import darkyenus.riverpebbleframework.flowui.{UIElement, TransitionOperator}
import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 13/04/14
 * Time: 15:50
 */
class MoveInOperator(direction:Vector2,scissor:Boolean = true) extends TransitionOperator {

  override def drawMidTransition(progress: Float, from: UIElement, to: UIElement, x: Float, y: Float, width: Float, height: Float, context: Render2DContext){
    from.process(0f,x,y,width,height,context)(Seq())
    val movingInX = x - direction.x * (1f-progress) * width
    val movingInY = y - direction.y * (1f-progress) * height
    var pushedScissor = false
    if(scissor){
      pushedScissor = context.pushScissor(x,y,width,height)
    }
    to.process(0f,movingInX,movingInY,width,height,context)(Seq())
    context.flush()
    if(pushedScissor){
      context.popScissor()
    }
  }
}
