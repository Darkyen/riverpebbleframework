package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 11/04/14
 * Time: 17:19
 */
trait UIElement {
  def width:Size = Fill
  def height:Size = Fill
  def process(delta:Float,x:Float,y:Float,width:Float,height:Float,context:Render2DContext)(implicit events:Seq[Event])

  protected def processEvents(eventProcessor:PartialFunction[Event,Unit],autoConsume:Boolean = true)(implicit events:Seq[Event]){
    for(event <- events if !event.consumed){
      if(eventProcessor.isDefinedAt(event)){
        eventProcessor(event)
        if(autoConsume){
          event.consume()
        }
      }
    }
  }
}
