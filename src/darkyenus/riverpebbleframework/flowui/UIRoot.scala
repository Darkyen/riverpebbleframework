package darkyenus.riverpebbleframework.flowui

import com.badlogic.gdx.{Gdx, InputAdapter}
import scala.collection.mutable
import com.badlogic.gdx.utils.TimeUtils
import scala.collection.mutable.ArrayBuffer
import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 11/04/14
 * Time: 23:43
 */
class UIRoot(root:UIElement,tapTolerance:Int = 4) extends InputAdapter {

  def process(context:Render2DContext,delta:Float = Gdx.graphics.getDeltaTime){
    process(delta,context,0f,0f,context.width,context.height)
  }

  def process(delta:Float,context:Render2DContext,x:Float,y:Float,width:Float,height:Float){
    updateInputEvents(delta)
    root.process(delta,x,y,width,height,context)(events)
    events.clear()
  }


  //Event resolution
  private val events = new ArrayBuffer[Event]()
  private class PointerInfo(var tappedAtX:Int = 0,var tappedAtY:Int = 0,var stillJustTap:Boolean = true,var tapStart:Long = 0)
  private val pointers = new mutable.HashMap[Int,PointerInfo]()

  def toSceneX(x:Int):Float = x.toFloat/Gdx.graphics.getWidth
  def toSceneY(y:Int):Float = y.toFloat/Gdx.graphics.getWidth

  override def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = {
    val info = pointers(pointer)
    if(info.stillJustTap){
      val xDistance = math.abs(info.tappedAtX - screenX)
      val yDistance = math.abs(info.tappedAtY - screenY)
      if(xDistance > tapTolerance || yDistance > tapTolerance){
        info.stillJustTap = false
        events.append(new BeginDragEvent(toSceneX(screenX),toSceneY(screenY),toSceneX(info.tappedAtX),toSceneY(info.tappedAtY),pointer))
        info.tappedAtX = screenX
        info.tappedAtY = screenY
      }
    }else if(!info.stillJustTap){
      events.append(new DragEvent(toSceneX(screenX),toSceneY(screenY),toSceneX(info.tappedAtX),toSceneY(info.tappedAtY),pointer))
      info.tappedAtX = screenX
      info.tappedAtY = screenY
    }
    true
  }

  override def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
    events.append(new ReleaseEvent(toSceneX(screenX),toSceneY(screenY),pointer,button))
    val info = pointers.remove(pointer).get
    if(info.stillJustTap){
      events.append(new TapEvent(toSceneX(info.tappedAtX),toSceneY(info.tappedAtY),pointer,button,(TimeUtils.millis() - info.tapStart)/1000f))
    }else{
      events.append(new EndDragEvent(toSceneX(screenX),toSceneY(screenY),pointer))
    }
    true
  }

  override def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
    val info = pointers.getOrElseUpdate(pointer,new PointerInfo())
    info.tappedAtX = screenX
    info.tappedAtY = screenY
    info.stillJustTap = true
    info.tapStart = TimeUtils.millis()
    events.append(new PressEvent(toSceneX(screenX),toSceneY(screenY),pointer,button))
    true
  }

  def updateInputEvents(delta:Float){
    for((pointer,info) <- pointers){
      if(info.stillJustTap){
        events.append(new TapContinuesEvent(toSceneX(info.tappedAtX),toSceneY(info.tappedAtY),pointer,delta,(TimeUtils.millis() - info.tapStart)/1000f))
      }
    }
  }
}
