package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.flowui.Orientation.Orientation

/**
 * Private property.
 * User: Darkyen
 * Date: 11/04/14
 * Time: 17:19
 */
trait Size {}

object Fill extends Size

case class Absolute(value:Float) extends Size

case class Fraction(fraction:Float, dimension:Orientation) extends Size {
  def apply(width:Float,height:Float):Float = {
    dimension match {
      case Orientation.Horizontal =>
        width * fraction
      case Orientation.Vertical =>
        height * fraction
    }
  }
}
