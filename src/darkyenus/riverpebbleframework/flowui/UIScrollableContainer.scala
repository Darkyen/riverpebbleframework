package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 13/04/14
 * Time: 18:08
 */
class UIScrollableContainer(element:UIMinimalSizeElement) extends UIElement {

  private var scrollX = 0f
  private var scrollY = 0f
  var lastScrollBarSizeX = 0f
  var lastScrollBarSizeY = 0f

  override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
    val minimalWidth = element.minimalWidth(width,height).getOrElse(0f)
    val minimalHeight = element.minimalHeight(width,height).getOrElse(0f)
    val willScrollX:Boolean = width < minimalWidth
    val willScrollY:Boolean = height < minimalHeight

    lastScrollBarSizeX = if(willScrollX){ (width / (minimalWidth + width)) min 1f }else 1f
    lastScrollBarSizeY = if(willScrollY){ (height / (minimalHeight + height)) min 1f}else 1f

    val elementX = if(willScrollX){
      x + scrollX * (width - minimalWidth)
    }else{
      scrollX = 0f
      x
    }

    val elementY = if(willScrollY){
      y + scrollY * (height - minimalHeight)
    }else{
      scrollY = 0f
      y
    }

    val elementWidth = if(willScrollX){
      minimalWidth
    }else{
      width
    }

    val elementHeight = if(willScrollY){
      minimalHeight
    }else{
      height
    }

    val pushed = if(willScrollX || willScrollY){
      context.spriteBatch.flush()
      context.pushScissor(x,y,width,height)
    }else false

    element.process(delta,elementX,elementY,elementWidth,elementHeight,context)(events)

    if(pushed){
      context.spriteBatch.flush()
      context.popScissor()
    }
  }

  def scrollXFraction:Float = scrollX

  def scrollYFraction:Float = scrollY

  def scrollXFraction_=(fraction:Float){
    scrollX = fraction min 1f max 0f
  }

  def scrollYFraction_=(fraction:Float){
    scrollY = fraction min 1f max 0f
  }
}

trait UIMinimalSizeElement extends UIElement {
  def minimalWidth(width:Float,height:Float):Option[Float]
  def minimalHeight(width:Float,height:Float):Option[Float]
}
