package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 13/04/14
 * Time: 17:17
 */
abstract class UIElementDelegate extends UIElement {

  def delegating:UIElement

  override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
    delegating.process(delta,x,y,width,height,context)(events)
  }

  override def height: Size = delegating.height

  override def width: Size = delegating.width
}
