package darkyenus.riverpebbleframework.flowui

import darkyenus.riverpebbleframework.Render2DContext

/**
 * Private property.
 * User: Darkyen
 * Date: 17/04/14
 * Time: 07:41
 */
abstract class UIScrollBar(val orientation:Orientation.Value) extends UIElement {

  def barSize(width:Float,height:Float):Float

  def barPosition:Float
  def barPosition_=(value:Float)

  def renderBackground(x: Float, y: Float, width: Float, height: Float, context: Render2DContext)
  def renderBar(x: Float, y: Float, width: Float, height: Float, context: Render2DContext)

  private var beingDragged = false

  override def process(delta: Float, x: Float, y: Float, width: Float, height: Float, context: Render2DContext)(implicit events: Seq[Event]){
    renderBackground(x,y,width,height,context)

    val barSize = this.barSize(width,height)

    @inline
    def dragMe(deltaX:Float,deltaY:Float){
      orientation match {
        case Orientation.Horizontal =>
          barPosition = ((deltaX/width)/barSize + barPosition) min 1f max 0f
        case Orientation.Vertical =>
          barPosition = ((deltaY/height)/barSize + barPosition)  min 1f max 0f
      }
    }

    processEvents({
      case BeginDragEvent(dragX,dragY,lastDragX,lastDragY,pointer) if x <= lastDragX && y <= lastDragY && x + width >= lastDragX && y + height >= lastDragY =>
        beingDragged = true
        dragMe(dragX - lastDragX,dragY - lastDragY)
      case DragEvent(dragX,dragY,lastDragX,lastDragY,pointer) if beingDragged =>
        dragMe(dragX - lastDragX,dragY - lastDragY)
      case EndDragEvent(dragX,dragY,pointer) if beingDragged =>
        beingDragged = false
    })
    orientation match {
      case Orientation.Horizontal =>
        val barX = x + (width - (barSize * width))*barPosition
        val barY = y
        val barWidth = width * barSize
        val barHeight = height
        renderBar(barX,barY,barWidth,barHeight,context)
      case Orientation.Vertical =>
        val barX = x
        val barY = y + (height - (barSize * height))*barPosition
        val barWidth = width
        val barHeight = height * barSize
        renderBar(barX,barY,barWidth,barHeight,context)
    }
  }
}
