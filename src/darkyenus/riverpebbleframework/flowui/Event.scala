package darkyenus.riverpebbleframework.flowui

/**
 * Private property.
 * User: Darkyen
 * Date: 13/03/14
 * Time: 18:09
 */
trait Event {
  private var consumed_ = false
  def consumed:Boolean = consumed_
  def consume(){
    consumed_ = true
  }
}

object Event {
  @inline
  def isContainedIn(x:Float,y:Float,boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    if(boxWidth < 0){
      isContainedIn(x,y,boxX+boxWidth,boxY,-boxWidth,boxHeight)
    }else if(boxHeight < 0){
      isContainedIn(x,y,boxX,boxY+boxHeight,boxWidth,-boxHeight)
    }else{
      boxX < x && boxY < y && boxX + boxWidth > x && boxY + boxHeight > y
    }
  }
}

case class PressEvent(x:Float,y:Float,pointer:Int,button:Int) extends Event {
  def isContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(x,y,boxX,boxY,boxWidth,boxHeight)
  }
}
case class ReleaseEvent(x:Float,y:Float,pointer:Int,button:Int) extends Event {
  def isContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(x,y,boxX,boxY,boxWidth,boxHeight)
  }
}
case class TapEvent(x:Float,y:Float,pointer:Int,button:Int,duration:Float) extends Event {
  def isContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(x,y,boxX,boxY,boxWidth,boxHeight)
  }
}
case class TapContinuesEvent(x:Float,y:Float,pointer:Int,durationDelta:Float,totalDuration:Float) extends Event {
  def isContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(x,y,boxX,boxY,boxWidth,boxHeight)
  }
}
case class DragEvent(x:Float,y:Float,lastX:Float,lastY:Float,pointer:Int) extends Event {
  def isContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(x,y,boxX,boxY,boxWidth,boxHeight)
  }
  def isOriginContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(lastX,lastY,boxX,boxY,boxWidth,boxHeight)
  }
}
case class BeginDragEvent(x:Float,y:Float,lastX:Float,lastY:Float,pointer:Int) extends Event {
  def isContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(x,y,boxX,boxY,boxWidth,boxHeight)
  }
  def isOriginContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(lastX,lastY,boxX,boxY,boxWidth,boxHeight)
  }
}
case class EndDragEvent(x:Float,y:Float,pointer:Int) extends Event {
  def isContainedIn(boxX:Float,boxY:Float,boxWidth:Float,boxHeight:Float):Boolean = {
    Event.isContainedIn(x,y,boxX,boxY,boxWidth,boxHeight)
  }
}

