package darkyenus.riverpebbleframework.entity

import scala.collection.mutable
import scala.reflect.ClassTag
import darkyenus.riverpebbleframework.entity.util.EntitySystemProxy
import com.esotericsoftware.kryo.{Kryo, KryoSerializable}
import com.esotericsoftware.kryo.io.{Output, Input}

/**
 * Private property.
 * User: Darkyen
 * Date: 22/01/14
 * Time: 17:49
 */
class EntityManager(entitySystemsRaw: Seq[EntitySystem] = Nil, val initialMaxEntities: Int = 128, private var timestamp: Long = 0) extends KryoSerializable {

  def this(entitySystemsRaw: EntitySystem*) {
    this(entitySystemsRaw)
  }

  private var lastComponentID = -1
  private val componentIDMap = new mutable.HashMap[Class[_ <: Component], Int]()
  private var entities = new Array[(Long, Map[Class[_ <: Component], Component])](initialMaxEntities)
  private val entitySystems: Array[(EntitySystem, Iterable[Entity])] = {
    Array.tabulate(entitySystemsRaw.length)(system =>{
      val flag = entitySystemsRaw(system).requiredComponents.foldLeft(0l)((flag, comp) => flag | (1 << getComponentID(comp)))
      (entitySystemsRaw(system), filteredIterable(flag))
    })
  }
  private lazy val entitySystemMap:Map[Class[_],EntitySystem] = entitySystemsRaw.map({
    case proxy:EntitySystemProxy =>
      (proxy.proxied.getClass,proxy.proxied)
    case sys:EntitySystem =>
      (sys.getClass,sys)
  }).toMap
  private var entityCount = 0
  private val freeSlots = mutable.SortedSet[Int]()

  private val delegate = new EntityManagerDelegate {

    private var toAdd: List[Iterable[Component]] = Nil

    private var toAddWithHook: List[(Iterable[Component], (Entity) => Unit)] = Nil

    private var toRemove: List[Entity] = Nil

    override def getComponents(entityId: Entity): Iterable[Component] = EntityManager.this.getEntity(entityId)

    override def getComponent[T <: Component](entityId: Entity)(implicit componentType: ClassTag[T]): T = EntityManager.this.getEntityComponent(entityId, componentType.runtimeClass.asInstanceOf[Class[T]])

    override def getComponent[T <: Component](entityId: Entity, componentType: Class[T]): T = EntityManager.this.getEntityComponent(entityId, componentType)

    override def hasComponent(entityId: Entity, componentType: Class[_ <: Component]): Boolean = EntityManager.this.hasEntityComponent(entityId, componentType)

    override def hasComponent[T <: Component](entityId: Entity)(implicit componentType: ClassTag[T]): Boolean = {
      hasComponent(entityId, componentType.runtimeClass.asInstanceOf[Class[T]])
    }

    override def removeEntity(entityId: Entity) {
      toRemove = entityId :: toRemove
    }

    override def addEntity(onAddHook: (Entity) => Unit, components: Component*) {
      addEntity(onAddHook, components)
    }

    override def addEntity(components: Component*) {
      addEntity(components)
    }

    override def addEntity(onAddHook: (Entity) => Unit, components: Iterable[Component]) {
      toAddWithHook = (components, onAddHook) :: toAddWithHook
    }

    override def addEntity(components: Iterable[Component]) {
      toAdd = components :: toAdd
    }

    override def hasEntity(entityId: Entity): Boolean = EntityManager.this.hasEntity(entityId)

    override protected[entity] def flush() {
      for (removing <- toRemove) {
        EntityManager.this.removeEntity(removing)
      }
      toRemove = Nil

      for (adding <- toAdd) {
        EntityManager.this.addEntity(adding)
      }
      toAdd = Nil

      for ((adding, hook) <- toAddWithHook) {
        hook(EntityManager.this.addEntity(adding))
      }
      toAddWithHook = Nil
    }

    override def filterEntities(requiredComponents: Iterable[Class[_ <: Component]]): Iterable[Entity] = EntityManager.this.filterEntities(requiredComponents)

    override def getSystem[T <: EntitySystem](implicit classTag: ClassTag[T]): Option[T] = EntityManager.this.getSystem(classTag)
  }

  private def filteredIterable(reqFlag: Long): Iterable[Entity] = {
    if (reqFlag == 0) {
      new Iterable[Entity] {
        override def iterator: Iterator[Entity] = new Iterator[Entity] {
          override def next(): Entity = throw new NoSuchElementException

          override def hasNext: Boolean = false
        }
      }
    } else {
      new Iterable[Entity] {
        override def iterator: Iterator[Entity] = new Iterator[Entity] {

          var nextReady = false
          var nextElement: Entity = new Entity(-1)

          def findNext() {
            nextReady = false
            do {
              nextElement = new Entity(nextElement.entityID + 1)
              if (entities(nextElement.entityID) != null) {
                if ((entities(nextElement.entityID)._1 & reqFlag) == reqFlag) {
                  nextReady = true
                }
              }
            } while (!nextReady && nextElement.entityID < entityCount + freeSlots.size)
          }

          findNext()

          override def next(): Entity = {
            val result = nextElement
            findNext()
            result
          }

          override def hasNext: Boolean = nextReady
        }
      }
    }
  }

  private val unfilteredIterable: Iterable[Int] = new Iterable[Int] {
    override def iterator: Iterator[Int] = new Iterator[Int] {

      var nextReady = false
      var nextElement: Int = -1

      def findNext() {
        nextReady = false
        do {
          nextElement += 1
          if (entities(nextElement) != null) {
            nextReady = true
          }
        } while (!nextReady && nextElement < entityCount + freeSlots.size)
      }

      findNext()

      override def next(): Int = {
        val result = nextElement
        findNext()
        result
      }

      override def hasNext: Boolean = nextReady
    }
  }

  def addEntity(components: Component*): Entity = {
    addEntity(components)
  }

  def addEntity(components: Iterable[Component]): Entity = {
    var newEntityID = entityCount
    if (!freeSlots.isEmpty) {
      newEntityID = freeSlots.head
      freeSlots -= newEntityID
    }
    entityCount += 1
    if (newEntityID == entities.length) {
      val oldEntities = entities
      entities = new Array[(Long, Map[Class[_ <: Component], Component])](entities.length * 2)
      System.arraycopy(oldEntities, 0, entities, 0, oldEntities.length)
    }
    val componentFlag = components.foldLeft(0l)((flag, comp) => flag | (1 << getComponentID(comp)))
    val componentMap = components.map(_.getComponentClass).zip(components).toMap
    entities(newEntityID) = (componentFlag, componentMap)
    //Return Entity wrapper
    new Entity(newEntityID)
  }

  def getEntity(id: Entity): Iterable[Component] = {
    if (hasEntity(id)) {
      entities(id.entityID)._2.values
    } else {
      Iterable.empty[Component]
    }
  }

  def hasEntity(id: Entity): Boolean = {
    if (entityCount + freeSlots.size > id.entityID) {
      entities(id.entityID) != null
    } else {
      false
    }
  }

  def removeEntity(id: Entity): Iterable[Component] = {
    if (hasEntity(id)) {
      entityCount -= 1
      val result = entities(id.entityID)._2.values
      entities(id.entityID) = null
      if (id.entityID == (entityCount + freeSlots.size)) {
        while (!freeSlots.isEmpty && freeSlots.last == (entityCount + freeSlots.size - 1)) {
          freeSlots -= freeSlots.last
        }
      } else {
        freeSlots += id.entityID
      }
      result
    } else {
      Iterable.empty[Component]
    }
  }

  def hasEntityComponent(id: Entity, componentType: Class[_ <: Component]): Boolean = {
    if (hasEntity(id)) {
      val flag = entities(id.entityID)._1
      val flagBit = 1 << getComponentID(componentType)
      (flagBit & flag) == flagBit
    } else {
      false
    }
  }

  def getEntityComponent[T <: Component](id: Entity, componentType: Class[T]): T = {
    if (hasEntity(id)) {
      entities(id.entityID)._2(componentType).asInstanceOf[T]
    } else {
      throw new IllegalArgumentException("Entity " + id + " does not exist.")
    }
  }

  def getEntityComponent[T <: Component](id: Entity)(implicit componentType: ClassTag[T]): T = {
    entities(id.entityID)._2(componentType.runtimeClass.asInstanceOf[Class[T]]).asInstanceOf[T]
  }

  def getEntityComponents(id:Entity):Iterable[Component] = {
    entities(id.entityID)._2.values
  }

  def filterEntities(requiredComponents: Iterable[Class[_ <: Component]]): Iterable[Entity] = {
    val componentFlag = requiredComponents.foldLeft(0l)((flag, comp) => flag | (1 << getComponentID(comp)))
    filteredIterable(componentFlag)
  }

  def getSystem[T <: EntitySystem](implicit classTag:ClassTag[T]):Option[T] = {
    entitySystemMap.get(classTag.runtimeClass).asInstanceOf[Option[T]] //This is actually needed
  }

  def getSystem[T <: EntitySystem](systemClass:Class[T]):Option[T] = {
    entitySystemMap.get(systemClass).asInstanceOf[Option[T]] //This is actually needed
  }

  def getSystems[T](implicit classTag:ClassTag[T]):Iterable[T with EntitySystem] = {
    val desiredClass = classTag.runtimeClass
    entitySystemMap.values.filter(desiredClass.isInstance(_)).asInstanceOf[Iterable[T with EntitySystem]]
  }

  def getEntityCount: Int = entityCount

  def getReusableIndexesCount: Int = freeSlots.size

  def process(delta: Float) {
    for ((entitySystem, iterable) <- entitySystems) {
      entitySystem.process(delta, timestamp, iterable)(delegate)
    }
    delegate.flush()
    advanceCountdownData(delta)
    timestamp += 1
  }

  private def advanceCountdownData(delta: Float) {
    for (entity <- unfilteredIterable) {
      for (component <- entities(entity)._2.values) {
        for (data <- component.components) {
          data match {
            case countdownData: CountdownData =>
              countdownData.countdown(delta)
            case _ =>
          }
        }
      }
    }
  }

  def getComponentID(component: Component): Int = {
    component.getComponentClass.componentID
  }

  def getComponentID(component: Class[_ <: Component]): Int = {
    component.componentID
  }

  private implicit class ComponentWithId(component: Class[_ <: Component]) {
    lazy val componentID: Int = {
      componentIDMap.getOrElseUpdate(component, {
        lastComponentID += 1
        lastComponentID
      })
    }
  }

  override def write(kryo: Kryo, output: Output){
    output.writeInt(getEntityCount,true)//WRITE ENTITY COUNT +I
    for(entity <- unfilteredIterable){// -1 = All ones
      val components = getEntityComponents(new Entity(entity)).toSeq
      output.writeByte(components.size)//WRITE COMPONENT COUNT B
      for(component <- components){
        kryo.writeClassAndObject(output,component)//WRITE COMPONENT C+O
      }
    }
  }

  override def read(kryo: Kryo, input: Input){
    val entityCount = input.readInt(true) //READ ENTITY COUNT +I
    for(i <- 0 until entityCount){
      val componentCount = input.readByte() //READ COMPONENT COUNT
      val components = Array.fill[Component](componentCount)(
        kryo.readClassAndObject(input).asInstanceOf[Component] //READ COMPONENT C+O
      )
      addEntity(components)
    }
  }
}