package darkyenus.riverpebbleframework.entity


/**
 * Private property.
 * User: Darkyen
 * Date: 12/02/14
 * Time: 23:13
 */
trait EntitySystem {

  val requiredComponents:Iterable[Class[_ <: Component]]

  def process(delta:Float,timestamp:Long,entities:Iterable[Entity])(implicit manager:EntityManagerDelegate)
}
