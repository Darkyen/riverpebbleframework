package darkyenus.riverpebbleframework.entity

import scala.reflect.ClassTag

/**
 * Private property.
 * User: Darkyen
 * Date: 15/02/14
 * Time: 22:57
 */
trait EntityManagerDelegate {

  def hasEntity(entityId:Entity):Boolean

  def addEntity(components:Component*)

  def addEntity(onAddHook:(Entity)=>Unit,components:Component*)

  def addEntity(components:Iterable[Component])

  def addEntity(onAddHook:(Entity)=>Unit,components:Iterable[Component])

  def removeEntity(entityId:Entity)

  def hasComponent(entityId:Entity,componentType:Class[_ <: Component]):Boolean

  def hasComponent[T <: Component](entityId:Entity)(implicit componentType:ClassTag[T]):Boolean

  def getComponent[T <: Component](entityId:Entity,componentType:Class[T]):T

  def getComponent[T <: Component](entityId:Entity)(implicit componentType:ClassTag[T]):T

  def getComponents(entityId:Entity):Iterable[Component]

  def getSystem[T <: EntitySystem](implicit classTag:ClassTag[T]):Option[T]

  def filterEntities(requiredComponents:Iterable[Class[_ <: Component]]):Iterable[Entity]

  protected[entity] def flush()
}
