package darkyenus.riverpebbleframework.entity.systems

import darkyenus.riverpebbleframework.entity.{Entity, EntityManagerDelegate, Component, EntitySystem}
import darkyenus.riverpebbleframework.entity.components.{Moving, Positioned}

/**
 * Private property.
 * User: Darkyen
 * Date: 16/02/14
 * Time: 13:02
 */
@Deprecated
class PhysicsSystem extends EntitySystem {
  override def process(delta: Float, timestamp: Long, entities: Iterable[Entity])(implicit manager: EntityManagerDelegate){
    for(entity <- entities){
      val position = manager.getComponent[Positioned](entity)
      val velocity = manager.getComponent[Moving](entity)

      position.position() = velocity.velocity().cpy().scl(delta).add(position.position())
      position.angle() = position.angle() + velocity.velocityAngular() * delta
    }
  }

  override val requiredComponents: Iterable[Class[_ <: Component]] = Iterable(classOf[Moving],classOf[Positioned])
}
