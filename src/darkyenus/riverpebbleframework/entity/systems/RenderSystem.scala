package darkyenus.riverpebbleframework.entity.systems

import darkyenus.riverpebbleframework.entity.{Entity, EntityManagerDelegate, Component, EntitySystem}
import darkyenus.riverpebbleframework.Render2D
import com.badlogic.gdx.Gdx
import darkyenus.riverpebbleframework.entity.components.{Positioned, Renderable}

/**
 * Private property.
 * User: Darkyen
 * Date: 16/02/14
 * Time: 14:08
 */
@Deprecated
class RenderSystem(render:Render2D) extends EntitySystem {

  override def process(delta: Float, timestamp: Long, entities: Iterable[Entity])(implicit manager: EntityManagerDelegate){
    render(Gdx.graphics.getWidth)(context => {
      val sortedEntities = entities.map(entity => (manager.getComponent[Renderable](entity),manager.getComponent[Positioned](entity),entity)).toSeq.sorted(renderPassOrdering)
      for((render,position,entity) <- sortedEntities){
        render.render()(position.getX,position.getY,position.angle(),delta,context,entity.entityID,manager)
      }
    })
  }

  private val renderPassOrdering = Ordering.by[(Renderable, Positioned,Entity), Int](_._1.renderPass())

  override val requiredComponents: Iterable[Class[_ <: Component]] = Iterable(classOf[Renderable],classOf[Positioned])
}
