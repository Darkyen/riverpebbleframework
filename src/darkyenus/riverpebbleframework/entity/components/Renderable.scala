package darkyenus.riverpebbleframework.entity.components

import darkyenus.riverpebbleframework.entity.{EntityManagerDelegate, Component}
import darkyenus.riverpebbleframework.Render2DContext
import darkyenus.riverpebbleframework.data.Data

/**
 * Private property.
 * User: Darkyen
 * Date: 16/02/14
 * Time: 12:59
 */
@Deprecated
class Renderable extends Component {

  val render:Data[Render] = new Render {
    override def apply(x: Float, y: Float, angle: Float, delta: Float, context: Render2DContext, me: Int, manager: EntityManagerDelegate){}
  }

  val renderPass:Data[Int] = 0

  def this(renderPass:Int,render:Render){
    this()
    this.renderPass() = renderPass
    this.render() = render
  }
}

/**
 * Float - x
 * Float - y
 * Float - angle
 * Float - delta
 * Render2DContext - context
 */
@Deprecated
trait Render extends ((Float,Float,Float,Float,Render2DContext,Int,EntityManagerDelegate) => Unit) {
  def apply(x: Float, y: Float, angle: Float, delta: Float, context: Render2DContext, me:Int, manager:EntityManagerDelegate)
}
