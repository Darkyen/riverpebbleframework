package darkyenus.riverpebbleframework.entity.components

import darkyenus.riverpebbleframework.entity.Component
import com.badlogic.gdx.math.Vector2
import darkyenus.riverpebbleframework.data.Data

/**
 * Private property.
 * User: Darkyen
 * Date: 16/02/14
 * Time: 12:56
 */
@Deprecated
class Moving extends Component {
  val velocity:Data[Vector2] = Vector2.Zero
  val velocityAngular:Data[Float] = 0f

  def this(velocityX:Float, velocityY:Float, velocityAngular:Float){
    this()
    this.velocity() = new Vector2(velocityX, velocityY)
    this.velocityAngular() = velocityAngular
  }
}
