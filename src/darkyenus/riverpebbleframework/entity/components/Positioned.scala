package darkyenus.riverpebbleframework.entity.components

import darkyenus.riverpebbleframework.entity.Component
import com.badlogic.gdx.math.Vector2
import darkyenus.riverpebbleframework.data.Data

/**
 * Private property.
 * User: Darkyen
 * Date: 16/02/14
 * Time: 12:57
 */
@Deprecated
class Positioned extends Component {
  val position:Data[Vector2] = new Vector2()
  val angle:Data[Float] = 0f

  def this(x:Float,y:Float,angle:Float){
    this()
    this.position() = new Vector2(x, y)
    this.angle() = angle
  }

  def this(position:Vector2,angle:Float){
    this()
    this.position() = position
    this.angle() = angle
  }

  def this(positioned:Positioned){
    this(positioned.position(),positioned.angle())
  }

  def getPosition:Vector2 = position().cpy()

  def getX:Float = position().x

  def getY:Float = position().y
}
