package darkyenus.riverpebbleframework.entity

import darkyenus.riverpebbleframework.data.{Data, DataHolder}

/**
 * Private property.
 * User: Darkyen
 * Date: 17/05/14
 * Time: 15:59
 */
object CountdownData {
   import scala.language.implicitConversions

   implicit def toCountdownDataConversion(anything:Float)(implicit component:DataHolder):CountdownData = {
     val result = new CountdownData(anything)
     component.addData(result)
     result
   }
 }

/**
  * This type of data gets automatically shrunk with time by amount of seconds passed.
  * For example if you save value 15 in CountdownData and the component is in active entity in properly updated
  * EntityManager, after 7 seconds, this data will have value of 8.
  * It only counts down until 0, there it stops and sits. The value never dips below 0.
  */
class CountdownData protected[entity]() extends Data[Float]() {

   def this(firstData:Float){
     this()
     update(firstData)
   }

   def countdown(time:Float){
     val value = this()
     if(value > 0){
       val newValue = value - time
       if(newValue < 0){
         this() = 0
       }else{
         this() = newValue
       }
     }
   }

 }