package darkyenus.riverpebbleframework.entity

import darkyenus.riverpebbleframework.data.DataHolder

/**
 * Private property.
 * User: Darkyen
 * Date: 08/02/14
 * Time: 10:45
 */
abstract class Component extends DataHolder {

  def components = data

  def getComponentClass:Class[_ <: Component] = getClass
}


