package darkyenus.riverpebbleframework.entity

import scala.reflect.ClassTag

/**
 * Private property.
 * User: Darkyen
 * Date: 24/04/14
 * Time: 21:24
 */
class Entity(val entityID:Int) extends AnyVal {

  @inline
  def hasComponent[C <: Component](implicit classTag:ClassTag[C], manager:EntityManagerDelegate):Boolean = {
    manager.hasComponent(this,classTag.runtimeClass.asInstanceOf[Class[C]])
  }

  @inline
  def getComponent[C <: Component](implicit classTag:ClassTag[C], manager:EntityManagerDelegate):C = {
    manager.getComponent[C](this)(classTag)
  }

  def getComponent[C <: Component](classTag:Class[C],manager:EntityManagerDelegate):C = {
    manager.getComponent[C](this,classTag)
  }

  def maybeGetComponent[C <: Component](implicit classTag:ClassTag[C], manager:EntityManagerDelegate):Option[C] = {
    if(hasComponent(classTag,manager)){
      Some(getComponent(classTag,manager))
    }else{
      None
    }
  }

}
