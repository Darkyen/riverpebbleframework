package darkyenus.riverpebbleframework.entity.util

import darkyenus.riverpebbleframework.entity.{Component, EntityManagerDelegate, Entity, EntitySystem}

/**
 * Private property.
 * User: Darkyen
 * Date: 03/05/14
 * Time: 12:01
 */
class EntitySystemProxy(val proxied:EntitySystem, enabled: =>Boolean) extends EntitySystem {
  override val requiredComponents: Iterable[Class[_ <: Component]] = proxied.requiredComponents

  override def process(delta: Float, timestamp: Long, entities: Iterable[Entity])(implicit manager: EntityManagerDelegate){
    if(enabled){
      proxied.process(delta,timestamp,entities)(manager)
    }
  }
}
