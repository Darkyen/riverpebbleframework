package darkyenus.riverpebbleframework.util

/**
 * Private property.
 * User: Darkyen
 * Date: 29/03/14
 * Time: 09:20
 */
class SlowValue(var value:Float, var target:Float, var speed:Float = 1f, var finishAction:()=>Unit = ()=>{}) {
  def apply():Float = value
  def update(target:Float){
    this.target = target
  }
  def process(delta:Float){
    if(value != target){
      val change = target - value
      val maxChange = delta * speed
      if(change.abs > maxChange){
        //Gap is bigger than this
        value += maxChange * change.signum
      }else{
        value = target
        finishAction()
      }
    }
  }

  def stable:Boolean = value == target
}

object SlowValue {
  def moveValueCloser(value:Float,target:Float,delta:Float,speed:Float):Float = {
    var result = value
    if(value != target){
      val change = target - value
      val maxChange = delta * speed
      if(change.abs > maxChange){
        //Gap is bigger than this
        result += maxChange * change.signum
      }else{
        result = target
      }
    }
    result
  }
}
