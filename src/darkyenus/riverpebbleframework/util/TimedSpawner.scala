package darkyenus.riverpebbleframework.util

import scala.util.Random

/*
 * Private property.
 * User: Darkyen
 * Date: 23/02/14
 * Time: 10:48
 */
/**
 * Class that fires event listener in random but controlled intervals.
 * Useful for spawning.
 * All times are intended to be seconds, but you can use anything.
 *
 * @param baseTime this is the base time that one fire will take
 * @param deviation Range in which baseTime can change
 * @param baseShift How is the deviation range positioned. 0 means only subtract random deviation,
 *                  1 means only add random deviation
 */
class TimedSpawner(var baseTime:Float,var deviation:Float,var baseShift:Float = 0.5f) {
  private var accumulator:Float = 0

  def update(delta:Float,fire:()=>Unit){
    accumulator -= delta
    while(accumulator <= 0){
      fire()
      accumulator += (baseTime + (0.5f-Random.nextFloat())*deviation)
    }
  }
}
