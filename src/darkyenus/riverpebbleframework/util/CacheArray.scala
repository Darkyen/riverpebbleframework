package darkyenus.riverpebbleframework.util

import scala.reflect.ClassTag

/**
 * Private property.
 * User: Darkyen
 * Date: 25/05/14
 * Time: 13:22
 */
class CacheArray[@specialized T](implicit classTag:ClassTag[T]) extends Iterable[T] {
  private var backingArray:Array[T] = new Array[T](16)
  private var contentSize = 0

  def add(item:T){
    if(contentSize == backingArray.length){
      resizeBackingArray(backingArray.length * 2)
    }
    backingArray(contentSize) = item
    contentSize += 1
  }

  private def resizeBackingArray(to:Int){
    val newBackingArray = new Array[T](to)
    System.arraycopy(backingArray,0,newBackingArray,0,backingArray.length)
    backingArray = newBackingArray
  }

  def addOrGetCached(defaultSet: =>T):T = {
    if(contentSize == backingArray.length){
      resizeBackingArray(backingArray.length * 2)
    }
    var existing = backingArray(contentSize)
    if(existing == null){
      existing = defaultSet
      backingArray(contentSize) = existing
    }
    contentSize += 1
    existing
  }

  def clear(){
    contentSize = 0
  }

  override def size:Int = contentSize

  override def iterator: Iterator[T] = new Iterator[T]{
    var position = -1
    override def hasNext: Boolean = position + 1 < contentSize

    override def next(): T = {
      position += 1
      backingArray(position)
    }
  }
}
