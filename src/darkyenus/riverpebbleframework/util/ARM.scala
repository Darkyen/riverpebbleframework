package darkyenus.riverpebbleframework.util

/**
 * Private property.
 * User: Darkyen
 * Date: 07/05/14
 * Time: 09:50
 */
object ARM {

  import scala.language.reflectiveCalls

  def using[T <: { def close() }]
  (resource: T)
  (block: T => Unit)
  {
    try {
      block(resource)
    } finally {
      if (resource != null) resource.close()
    }
  }
}
