package darkyenus.riverpebbleframework.util

import com.badlogic.gdx.math.{MathUtils, Interpolation}

/**
 * Private property.
 * User: Darkyen
 * Date: 08/05/14
 * Time: 13:59
 */
class PresetInterpolation(values:Seq[Float],interpolation:Interpolation = Interpolation.linear) extends Interpolation {

  assert(values.size > 0,"Invalid parameters.")

  private val sizeMinusOne = values.size - 1

  override def apply(a: Float): Float = {
    val indexFloat = a * sizeMinusOne
    val start = indexFloat.toInt max 0 min sizeMinusOne
    val end = (start + 1) min sizeMinusOne
    val spot = indexFloat % 1f max 0f
    interpolation(values(start),values(end),spot)
  }
}
