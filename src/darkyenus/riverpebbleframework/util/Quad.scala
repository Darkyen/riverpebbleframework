package darkyenus.riverpebbleframework.util

import com.badlogic.gdx.graphics.{Texture, Color}
import com.badlogic.gdx.graphics.g2d.{Batch, TextureRegion}

/**
 * Private property.
 * User: Darkyen
 * Date: 24/05/14
 * Time: 22:40
 */
class Quad {
  val vertices = new Array[Float](20)

  def position(x:Float,y:Float,width:Float,height:Float):Quad = {
    val x1 = x + width
    val y1 = y + height
    vertices(0) = x
    vertices(1) = y

    vertices(5) = x
    vertices(6) = y1

    vertices(10) = x1
    vertices(11) = y1

    vertices(15) = x1
    vertices(16) = y
    this
  }

  def texture():Quad = {
    vertices(3) = 0f
    vertices(4) = 1f

    vertices(8) = 0f
    vertices(9) = 0f

    vertices(13) = 1f
    vertices(14) = 0f

    vertices(18) = 1f
    vertices(19) = 1f
    this
  }

  def texture(tex:TextureRegion):Quad = {
    vertices(3) = tex.getU
    vertices(4) = tex.getV2

    vertices(8) = tex.getU
    vertices(9) = tex.getV

    vertices(13) = tex.getU2
    vertices(14) = tex.getV

    vertices(18) = tex.getU2
    vertices(19) = tex.getV2
    this
  }

  def screen():Quad = {
    vertices(0) = -1f
    vertices(1) = 1f

    vertices(5) = -1f
    vertices(6) = -1f

    vertices(10) = 1f
    vertices(11) = -1f

    vertices(15) = 1f
    vertices(16) = 1f
    this
  }

  def color(col:Color):Quad = {
    color(col.toFloatBits)
  }

  def color(col:Float):Quad = {
    vertices(2) = col
    vertices(7) = col
    vertices(12) = col
    vertices(17) = col
    this
  }

  def white():Quad = {
    vertices(2) = Quad.white
    vertices(7) = Quad.white
    vertices(12) = Quad.white
    vertices(17) = Quad.white
    this
  }

  def addBottom(x:Float,y:Float):Quad = {
    vertices(0) += x
    vertices(1) += y

    //vertices(5) += x
    //vertices(6) += y

    //vertices(10) += x
    //vertices(11) += y

    vertices(15) += x
    vertices(16) += y
    this
  }

  def addTop(x:Float,y:Float):Quad = {
    //vertices(0) += x
    //vertices(1) += y

    vertices(5) += x
    vertices(6) += y

    vertices(10) += x
    vertices(11) += y

    //vertices(15) += x
    //vertices(16) += y
    this
  }

  def addLeft(x:Float,y:Float):Quad = {
    vertices(0) += x
    vertices(1) += y

    vertices(5) += x
    vertices(6) += y

    //vertices(10) += x
    //vertices(11) += y

    //vertices(15) += x
    //vertices(16) += y
    this
  }

  def addRight(x:Float,y:Float):Quad = {
    //vertices(0) += x
    //vertices(1) += y

    //vertices(5) += x
    //vertices(6) += y

    vertices(10) += x
    vertices(11) += y

    vertices(15) += x
    vertices(16) += y
    this
  }

  def addTopLeft(x:Float,y:Float):Quad = {
    vertices(5) += x
    vertices(6) += y
    this
  }

  def addTopRight(x:Float,y:Float):Quad = {
    vertices(10) += x
    vertices(11) += y
    this
  }

  def addBottomLeft(x:Float,y:Float):Quad = {
    vertices(0) += x
    vertices(1) += y
    this
  }

  def addBottomRight(x:Float,y:Float):Quad = {
    vertices(15) += x
    vertices(16) += y
    this
  }

  def draw(batch:Batch,texture:Texture){
    batch.draw(texture,vertices,0,20)
  }
}

object Quad {
  private val white = Color.WHITE.toFloatBits
  private val applyQuad:Quad = new Quad
  @inline
  def apply():Quad = applyQuad
}
