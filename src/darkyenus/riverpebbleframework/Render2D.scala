package darkyenus.riverpebbleframework

import com.badlogic.gdx.graphics._
import com.badlogic.gdx.math.{Rectangle, Vector3, Vector2}
import com.badlogic.gdx.graphics.g2d.{TextureRegion, BitmapFont, SpriteBatch}
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack

/**
 * Private property.
 * User: Darkyen
 * Date: 20/12/13
 * Time: 19:56
 */
class Render2D(val spriteBatch:SpriteBatch = new SpriteBatch()) {

  private val camera = new OrthographicCamera()

  def this(parent:Render2D) = this(parent.spriteBatch)

  /**
   * Creates a render function for all your needs!
   *
   * @param unitSize a size of 1 unit in pixels
   * @param position to which camera should be centered
   * @param render function to render with
   */
  def apply(unitSize:Float = 1f,position:Vector2 = Vector2.Zero,willUseFrustum:Boolean = false,yDown:Boolean = true)(render:(Render2DContext)=>Unit){
    val screenWidth:Float = Gdx.graphics.getWidth/unitSize
    val screenHeight:Float = Gdx.graphics.getHeight/unitSize
    camera.setToOrtho(yDown,screenWidth,screenHeight)
    camera.translate(position)
    camera.update(willUseFrustum)
    spriteBatch.setProjectionMatrix(camera.combined)
    spriteBatch.begin()

    val unitSize0 = unitSize
    val position0 = position

    render(new Render2DContext {

      def spriteBatch: SpriteBatch = Render2D.this.spriteBatch

      def camera: OrthographicCamera = Render2D.this.camera

      def frustumReady: Boolean = willUseFrustum

      def position: Vector2 = position0

      def unitSize:Float = unitSize0

      override def width: Float = screenWidth

      override def height: Float = screenHeight
    })
    spriteBatch.end()
  }

  def width:Int = Gdx.graphics.getWidth

  def height:Int = Gdx.graphics.getHeight

}

object Render2D {
  def clearScreen(){
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
  }

  lazy val White:Texture = {
    val pixmap = new Pixmap(1,1,Pixmap.Format.RGB565)
    pixmap.drawPixel(0,0,Color.WHITE.toIntBits)
    new Texture(pixmap)
  }

  lazy val DefaultFont:BitmapFont = new BitmapFont(true)

  var resized = false
}

trait Render2DContext {

  /**
   * Translates given vector to world coordinates.
   *
   * @param screenCoordinates vector to change
   * @return the same vector for chaining
   * @see toScreenCoordinates
   */
  def toWorldCoordinates(screenCoordinates:Vector2):Vector2 = {
    //screenCoordinates.scl(1,-1).add(0,Gdx.graphics.getHeight)
    screenCoordinates.add(position)
    screenCoordinates.scl(1f/unitSize)
  }

  /**
   * Translates given vector to screen (input) coordinates.
   *
   * @param worldCoordinates vector to change
   * @return the same vector for chaining
   * @see toWorldCoordinates
   */
  def toScreenCoordinates(worldCoordinates:Vector2):Vector2 = {
    worldCoordinates.scl(unitSize)
    worldCoordinates.sub(position)
    //worldCoordinates.scl(1,-1).add(0,Gdx.graphics.getHeight)
  }

  def frustum:Rectangle = {
    val topLeft = new Vector2(0,0)
    val bottomRight = new Vector2(width,height)
    toWorldCoordinates(topLeft)
    toWorldCoordinates(bottomRight)
    bottomRight.sub(topLeft)
    new Rectangle(topLeft.x,topLeft.y,bottomRight.x,bottomRight.y)
  }

  def camera:OrthographicCamera

  def spriteBatch:SpriteBatch

  def frustumReady:Boolean

  def position:Vector2

  def unitSize:Float

  def apply(relativePosition:Vector2 = Vector2.Zero,willUseFrustum:Boolean = false)(render:(Render2DContext)=>Unit){
    val previousPosition = new Vector3(camera.position)
    camera.translate(relativePosition)
    camera.update(willUseFrustum)
    spriteBatch.end()
    spriteBatch.setProjectionMatrix(camera.combined)
    spriteBatch.begin()

    val spriteBatch0 = spriteBatch
    val camera0 = camera
    val position0 = position
    val unitSize0 = unitSize

    val width0 = width
    val height0 = height

    render(new Render2DContext {

      def spriteBatch: SpriteBatch = spriteBatch0

      def frustumReady: Boolean = willUseFrustum

      def camera: OrthographicCamera = camera0

      lazy val position: Vector2 = position0.cpy().add(relativePosition)

      def unitSize: Float = unitSize0

      override def width: Float = width0

      override def height: Float = height0
    })
    spriteBatch.end()
    camera.position.set(previousPosition)
    camera.update(frustumReady)
    spriteBatch.setProjectionMatrix(camera.combined)
    spriteBatch.begin()
  }

  def shaded[S <: ShaderProgram](shader:S)(render:(S)=>Unit){
    spriteBatch.setShader(shader)
    render(shader)
    spriteBatch.setShader(null)
  }

  private val scissorTempRect = new Rectangle()

  def pushScissor(x:Float,y:Float,width:Float,height:Float):Boolean = {
    scissorTempRect.set(x,y,width,height)
    pushScissor(scissorTempRect)
  }

  private val projectedScissorTempRect = new Rectangle()

  def pushScissor(scissor:Rectangle):Boolean = {
    ScissorStack.calculateScissors(camera,spriteBatch.getTransformMatrix,scissor,projectedScissorTempRect)
    ScissorStack.pushScissors(projectedScissorTempRect)
  }

  def popScissor(){
    ScissorStack.popScissors()
  }

  def scissored(x:Float,y:Float,width:Float,height:Float)(render:()=>Unit){
    val pushed = pushScissor(x,y,width,height)
    render()
    if(pushed)popScissor()
  }

  //Draw helper methods

  def width:Float

  def height:Float

  /**
   * Equals to height/width
   * Usually smaller than one
   */
  lazy val ratioHW = height/width

  /**
   * Equals to width/height
   * Usually bigger than one
   */
  lazy val ratioWH = width/height

  def draw(texture:Texture,x:Float,y:Float,width:Float,height:Float,rotation:Float = 0f,textureX:Int,textureY:Int,textureWidth:Int,textureHeight:Int){
    spriteBatch.draw(texture,x,y,x+width/2f,y+height/2f,width,height,1f,1f,rotation,textureX,textureY,textureWidth,textureHeight,false,true)
  }

  def drawCentered(font:BitmapFont,text:CharSequence,x:Float,y:Float):TextBounds = {
    val bounds = font.getBounds(text)
    font.draw(spriteBatch,text,x-bounds.width/2,y-bounds.height/2)
  }

  def drawCentered(texture:TextureRegion,x:Float,y:Float,width:Float,height:Float,rotation:Float){
    spriteBatch.draw(texture,x-width/2,y-height/2,width/2,height/2,width,height,1f,1f,rotation)
  }

  def drawCenteredW(texture:TextureRegion,x:Float,y:Float,width:Float,rotation:Float):Float = {
    val height = (texture.getRegionHeight*width)/texture.getRegionWidth
    spriteBatch.draw(texture,x-width/2,y-height/2,width/2,height/2,width,height,1f,1f,rotation)
    height
  }

  def drawCenteredH(texture:TextureRegion,x:Float,y:Float,height:Float,rotation:Float):Float = {
    val width = (texture.getRegionWidth*height)/texture.getRegionHeight
    spriteBatch.draw(texture,x-width/2,y-height/2,width/2,height/2,width,height,1f,1f,rotation)
    width
  }

  def drawW(texture:TextureRegion,x:Float,y:Float,width:Float,rotation:Float):Float = {
    val height = (texture.getRegionHeight*width)/texture.getRegionWidth
    spriteBatch.draw(texture,x,y,width/2,height/2,width,height,1f,1f,rotation)
    height
  }

  def drawH(texture:TextureRegion,x:Float,y:Float,height:Float,rotation:Float):Float = {
    val width = (texture.getRegionWidth*height)/texture.getRegionHeight
    spriteBatch.draw(texture,x,y,width/2,height/2,width,height,1f,1f,rotation)
    width
  }

  def drawCenteredXW(texture:TextureRegion,x:Float,y:Float,width:Float,rotation:Float):Float = {
    val height = (texture.getRegionHeight*width)/texture.getRegionWidth
    spriteBatch.draw(texture,x-width/2,y,width/2,height/2,width,height,1f,1f,rotation)
    height
  }

  def drawCenteredYW(texture:TextureRegion,x:Float,y:Float,width:Float,rotation:Float):Float = {
    val height = (texture.getRegionHeight*width)/texture.getRegionWidth
    spriteBatch.draw(texture,x,y-height/2,width/2,height/2,width,height,1f,1f,rotation)
    height
  }

  def drawCenteredXH(texture:TextureRegion,x:Float,y:Float,height:Float,rotation:Float):Float = {
    val width = (texture.getRegionWidth*height)/texture.getRegionHeight
    spriteBatch.draw(texture,x-width/2,y,width/2,height/2,width,height,1f,1f,rotation)
    width
  }

  def drawCenteredYH(texture:TextureRegion,x:Float,y:Float,height:Float,rotation:Float):Float = {
    val width = (texture.getRegionWidth*height)/texture.getRegionHeight
    spriteBatch.draw(texture,x,y-height/2,width/2,height/2,width,height,1f,1f,rotation)
    width
  }
}

object Render2DContext {
  import scala.language.implicitConversions
  @Deprecated
  implicit def toCamera(helper:Render2DContext):OrthographicCamera = helper.camera
  @Deprecated
  implicit def toSpriteBatch(helper:Render2DContext):SpriteBatch = helper.spriteBatch
}




