package darkyenus.riverpebbleframework.data.newdata

import com.badlogic.gdx.utils.{Array => Barray}
import scala.language.implicitConversions

/**
 * Private property.
 * User: Darkyen
 * Date: 20/08/14
 * Time: 13:52
 */
sealed trait Data {
  def write(out:DataOutput)
  def read(in:DataInput)
  var changed:Boolean = false
}

final class MapData[K,V] extends Data {
  override def write(out: DataOutput): Unit = ???

  override def read(in: DataInput): Unit = ???
}

final class SeqData[V] extends Data {

  private val value:Barray[V] = new Barray[V](true,8)

  @inline
  def apply(index:Int):V = {
    //TODO Check size
    value.get(index)
  }

  @inline
  def update(index:Int,value:V): Unit = {
    //TODO Check size
    changed = changed || (this.value.get(index) != value)
    this.value.set(index,value)
  }

  override def write(out: DataOutput): Unit = ???

  override def read(in: DataInput): Unit = ???
}

final class SetData[V] extends Data {

  private val value:Barray[V] = new Barray[V](false,8)

  def put(value:V): Unit ={
    com.badlogic.gdx.utils.async.ThreadUtils.`yield`()
  }

  override def write(out: DataOutput): Unit = ???

  override def read(in: DataInput): Unit = ???
}

final class KeyData[V] extends Data {

  private var value:V = _
  
  def this(initialValue:V){
    this()
    value = initialValue
  }

  @inline
  def apply():V = value

  @inline
  def update(value:V): Unit = {
    changed = changed || (this.value != value)
    this.value = value
  }

  override def write(out: DataOutput): Unit = ???

  override def read(in: DataInput): Unit = ???
}

object Data {
  implicit def toMapData[K,V](map:Map[K,V]):MapData[K,V] = {
    ???
  }

  implicit def toSeqData[V](seq:Seq[V]):SeqData[V] = {
    ???
  }

  implicit def toSetData[V](seq:Set[V]):SetData[V] = {
    ???
  }

  implicit def toKeyData[V](key:V):KeyData[V] = {
    new KeyData[V](key)
  }

  def map[K,V]():MapData[K,V] = ???

  def seq[V]():SeqData[V] = ???

  def set[V]():SetData[V] = ???

  def key[V]():KeyData[V] = new KeyData[V]()
}


