package darkyenus.riverpebbleframework.data

import com.esotericsoftware.kryo.io.{Input, Output}
import com.esotericsoftware.kryo.{Kryo, KryoSerializable}

/**
 * Private property.
 * User: Darkyen
 * Date: 17/05/14
 * Time: 15:56
 */
trait DataHolder extends KryoSerializable {

  implicit val $ = this

	protected def dataHolderDataSizeHint:Int = 0

  private var $data = new Array[Data[_]](dataHolderDataSizeHint)

  def addData(data:Data[_]){
    $data = $data :+ data
  }

	def addData(data:Data[_],ordinal:Byte){
		if($data.length > ordinal){
			if($data(ordinal) != null){
				println(s"${Console.RED}DataHolder: Data ordinal conflict! (For $ordinal in $this between first ${$data(ordinal)} and second $data)")
			}
			$data(ordinal) = data
		}else{
			val newLength = ordinal+1
			val newData = new Array[Data[_]](newLength)
			Array.copy($data,0,newData,0,$data.length)
			newData(ordinal) = data
			$data = newData
		}
	}

  def data = $data

	def write(kryo: Kryo, output: Output){
		DataHolder.SerializationStrategy match {
			case DataSerializationStrategy.Fast =>
				writeFast(kryo,output)
			case DataSerializationStrategy.Changeable =>
				writeChangeable(kryo,output)
		}
	}

	def read(kryo: Kryo, input: Input){
		DataHolder.SerializationStrategy match {
			case DataSerializationStrategy.Fast =>
				readFast(kryo,input)
			case DataSerializationStrategy.Changeable =>
				readChangeable(kryo,input)
		}
	}

  def writeFast(kryo: Kryo, output: Output){
    for(component <- $data){
      component.write(kryo,output)//TODO Null support
    }
  }

  def readFast(kryo: Kryo, input: Input){
    for(component <- $data){
      component.read(kryo,input)//TODO Null support
    }
  }

	def writeChangeable(kryo:Kryo, output: Output): Unit = {
		val bufferOut = DataHolder.ArrayOutput
		var index = 0
		val data = $data
		val dLength = data.length
		var existingCount = 0

		while(index < dLength){
			if(data(index) != null){
				existingCount += 1
			}
			index += 1
		}

		output.writeByte(existingCount)

		index = 0
		while(index < dLength){
			val value = data(index)
			if(value != null){
				output.writeByte(index)
				value.write(kryo,bufferOut)
				val bytesWritten = bufferOut.position()
				output.writeShort(bytesWritten)
				output.write(bufferOut.getBuffer,0,bytesWritten)
				bufferOut.clear()
			}
			index += 1
		}
	}

	def readChangeable(kryo:Kryo,input:Input): Unit = {
		val existingCount = input.readByte()
		for(_ <- 0 until existingCount){
			val index = input.readByte()
			val size = input.readShort()
			val bufferIn = DataHolder.arrayInput(size)
			input.read(bufferIn.getBuffer,0,size)
			if(index >= 0 && index < $data.length){
				bufferIn.setPosition(0)
				bufferIn.setLimit(size)
				$data(index).read(kryo,bufferIn)
			}
		}
	}
}

object DataHolder {
	var SerializationStrategy = DataSerializationStrategy.Changeable

	private val BaseSize = 64
	private val MaxSize = 8192

	private lazy val ArrayOutput = new Output(BaseSize,MaxSize)

	private lazy val ArrayInput = new Input(BaseSize)

	def arrayInput(requiredSize:Int):Input = {
		if(requiredSize <= ArrayInput.getBuffer.length){
			ArrayInput
		}else if(requiredSize > MaxSize){
			sys.error("Required buffer size is too big.")
		}else{
			var finalLength = ArrayInput.getBuffer.length * 2
			while(finalLength < requiredSize){
				finalLength *= 2
			}
			ArrayInput.setBuffer(new Array[Byte](finalLength))
			ArrayInput
		}
	}
}
