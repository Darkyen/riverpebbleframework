package darkyenus.riverpebbleframework.data

import com.esotericsoftware.kryo.{Kryo, KryoSerializable}
import com.esotericsoftware.kryo.io.{Input, Output}

/**
 *
 * This is a container for data to be used and serialized in components.
 * It is mutable, but it remembers that it has been changed.
 *
 * Private property.
 * User: Darkyen
 * Date: 08/02/14
 * Time: 10:46
 */
class Data[@specialized D] protected[data]() extends KryoSerializable {

  def this(firstData:D){
    this()
    data = firstData
  }

  private var data:D = _
  private var changed = true

  @inline
  def apply():D = data

  @inline
  def update(data:D){
    this.data = data
  }

  def pollChanged:Boolean = {
    if(changed){
      changed = false
      true
    }else{
      false
    }
  }

  def setChanged(){
    changed = true
  }

  def isChanged:Boolean = {
    changed
  }

  override def toString: String = {
    data.toString
  }

  override def read(kryo: Kryo, input: Input){
    data = kryo.readClassAndObject(input).asInstanceOf[D]
  }

  override def write(kryo: Kryo, output: Output){
    kryo.writeClassAndObject(output,data)
  }
}

object Data {
  import scala.language.implicitConversions
  implicit def toDataConversion[D](anything:D)(implicit dataHolder:DataHolder):Data[D] = {
    val result = new Data[D](anything)
    dataHolder.addData(result)
    result
  }

	implicit def toDataConversion[D](anything:(D,Int))(implicit dataHolder:DataHolder):Data[D] = {
		val result = new Data[D](anything._1)
		dataHolder.addData(result,anything._2.toByte)
		result
	}
}