package darkyenus.riverpebbleframework.data;

/**
 * Private property.
 * User: Darkyen
 * Date: 07/08/14
 * Time: 21:58
 */
public enum DataSerializationStrategy {
    /**
     * Only the bare minimum is serialized. This is fast and light, useful for network transfer.
     */
    Fast,
    /**
     * A real beast. Slower than Extensible and StructureGrowable combined, and bigger than them too.
     * But on the plus side, it support adding and removing Data's to your liking.
     * Changing their type is not supported though and will probably result in data corruption or read skip,
     * depends on the situation.
     */
    Changeable
}
