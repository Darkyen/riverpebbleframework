package darkyenus.riverpebbleframework.tools.resourcepacker

import java.awt._
import java.awt.image.BufferedImage
import java.io.{File, FileInputStream, FileOutputStream, FileReader}
import javax.imageio.ImageIO

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.tools.hiero.BMFontUtil
import com.badlogic.gdx.tools.hiero.unicodefont.effects.{ColorEffect, Effect}
import com.badlogic.gdx.tools.hiero.unicodefont.{HieroSettings, UnicodeFont}
import com.badlogic.gdx.tools.texturepacker.TexturePacker
import com.badlogic.gdx.utils.{Json, JsonReader}
import com.google.common.base.Charsets
import com.google.common.io.{ByteStreams, Files}
import org.apache.batik.transcoder.image.{ImageTranscoder, PNGTranscoder}
import org.apache.batik.transcoder.{SVGAbstractTranscoder, TranscoderInput, TranscoderOutput}
import org.lwjgl.LWJGLUtil

import scala.util.matching.Regex

/**
 * Private property.
 * User: Darkyen
 * Date: 16/07/14
 * Time: 19:23
 */
trait Task {

  type PostProcess = ()=>Unit

  type TaskFunction = PartialFunction[Resource,Unit]

  val process:TaskFunction

  def myTempFolder:File = new File(Task.TempFolder,getClass.getSimpleName.stripSuffix("$"))

  private def myTempFolderForFiles = new File(myTempFolder,"Files")

  private def myTempFolderForFolders = new File(myTempFolder,"Directories")

  def createTempFile(file:ResourceFile,extension:String = null):File = {
    var result:File = null
    val string = new StringBuilder
    var tempFileCounter = -1
    do{
      string.append(file.name).append('.')
      tempFileCounter += 1
	    if(tempFileCounter == 9)tempFileCounter = 10 //Hardcoding cornercases yay! (This shouldn't happen though)

      if(tempFileCounter != 0) {
        string.append(tempFileCounter).append('.')
      }
      file.flags.addString(string,".")
      string.append('.').append(if(extension == null)file.extension else extension)
      result = new File(myTempFolderForFiles,string.toString())
      string.clear()
    }while(result.exists())
	  Files.createParentDirs(result)
    result
  }

  var tempDirectoryNum = 0 //TODO not multiple run friendly

  def createTempDirectory():File = {
    var result:File = null
    do{
      result = new File(myTempFolderForFolders,"tempDir"+tempDirectoryNum)
      tempDirectoryNum += 1
    }while(result.exists())
    if(!result.mkdirs()){
      error("Could not create temporary directory.",result)
    }
    result
  }
}

object Task {
  lazy val All = Seq(CreateFontsTask,ConvertModelsTask,FlattenTask,ResizeTask,RasterizeTask,PreBlendTask,PackTask,RemoveEmptyDirectoriesTask)

  var TempFolder:File = null
  var TempFolderPath:String = null

  var options:Map[Symbol,String] = null

  @volatile
  private var tasksRunning = false

  def prepare(options:Map[Symbol,String]) {
    this.synchronized({
      assert(!tasksRunning)
      tasksRunning = true
      TempFolder = Files.createTempDir()
      TempFolderPath = TempFolder.getCanonicalPath
      this.options = options
    })
  }

  def end() {
    this.synchronized({
      assert(tasksRunning)
      if(options.contains('DEBUG)){
        Desktop.getDesktop.open(TempFolder)
      }else{
        clearFolder(TempFolder)
        TempFolder.delete()
        TempFolder = null
        TempFolderPath = null
      }
      options = null
      tasksRunning = false
    })
  }

  val json = new Json()
  val jsonReader = new JsonReader
}

object FlattenTask extends Task {
  override val process:TaskFunction = {
    case directory:ResourceDirectory if directory.flags.contains("flatten") =>
      flatten(directory)
      log("Directory flattened.",directory)
  }

  def flatten(directory:ResourceDirectory){
    //Grandparent will no longer acknowledge this child and take all his children. Harsh.
    val grandparent = directory.parent
    grandparent.removeChild(directory)
    directory.children.foreach(child => {
      directory.removeChild(child)
      grandparent.addChild(child)
    })
  }
}

object PreBlendTask extends Task {

	/**
	 * Matches: #RRGGBB
	 * Where RR (GG and BB) are hexadecimal digits.
	 * Example:
	 * #FF0056
	 * to capture groups RR GG BB
	 */
	val PreBlendRegex = "#([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])".r

	override val process: TaskFunction = {
		case image:ResourceFile if image.isImage =>
			image.flags collectFirst {
				case PreBlendRegex(r,g,b) =>
					val color = new Color(Integer.parseInt(r,16),Integer.parseInt(g,16),Integer.parseInt(b,16))
					val output = createTempFile(image)
					Files.copy(image.file,output)
					HelperTasks.preBlendImage(output,color,ninepatch = image.flags.contains("9"))
					image.file = output
			}
	}
}

object RasterizeTask extends Task {
	private val transcoder = new PNGTranscoder()

	val PixelSizePattern = """(\d+)x(\d+)""".r
	/*
	Example:
	450x789   => Image will be 450 pixels wide and 789 pixels tall
	 */

	val PixelWidthPattern = """(\d+)x""".r
	/*
	Example:
	450x   => Image will be 450 pixels wide and height will be inferred
	 */

	val PixelHeightPattern = """x(\d+)""".r
	/*
	Example:
	x789   => Image will be 789 pixels tall and width will be inferred
	 */

	override val process: TaskFunction = {
		case svg:ResourceFile if svg.extension.equalsIgnoreCase("svg") =>
			val resultFile = createTempFile(svg,"png")
			val in = new FileInputStream(svg.file)
			val input = new TranscoderInput(in)
			val out = new FileOutputStream(resultFile)
			val output = new TranscoderOutput(out)

			transcoder.getTranscodingHints.clear()
			transcoder.addTranscodingHint(ImageTranscoder.KEY_BACKGROUND_COLOR,new Color(0,0,0,0))
			svg.flags.collectFirst {
				case PixelSizePattern(width,height) =>
					transcoder.addTranscodingHint(SVGAbstractTranscoder.KEY_WIDTH,width.toInt.toFloat)
					transcoder.addTranscodingHint(SVGAbstractTranscoder.KEY_HEIGHT,height.toInt.toFloat)
				case PixelWidthPattern(width) =>
					transcoder.addTranscodingHint(SVGAbstractTranscoder.KEY_WIDTH,width.toInt.toFloat)
				case PixelHeightPattern(height) =>
					transcoder.addTranscodingHint(SVGAbstractTranscoder.KEY_HEIGHT,height.toInt.toFloat)
			}

			transcoder.transcode(input,output)
			out.flush()
			out.close()
			in.close()

			svg.parent.addChild(resultFile)
			svg.removeFromParent()
			log("Svg rasterized.",svg.name)
	}
}

object ResizeTask extends Task {

  val TileSizePattern = """w(\d+(?:,\d+)?)h(\d+(?:,\d+)?)""".r
  /*
  Examples:
  w15h1    => Image will be 15 tile-sizes wide and 1 tile size tall
  w1,5h0,5 => Image will be 1.5 tile-sizes wide and 0.5 tile size tall
   */
  val PixelSizePattern = """(\d+)x(\d+)""".r
  /*
  Example:
  450x789   => Image will be 450 pixels wide and 789 pixels tall
   */

  def tileSize = Task.options('TileSize).toInt //TODO Not multiple run friendly

  def tileFraction(input:String):Int = {
    (input.replace(',','.').toFloat * tileSize).round
  }

  override val process: TaskFunction = {
    case file:ResourceFile if file.isImage =>
      file.flags.collectFirst {
        case TileSizePattern(tileWidth,tileHeight) if tileWidth != null && tileHeight != null => (tileFraction(tileWidth),tileFraction(tileHeight))
        case PixelSizePattern(pixelWidth,pixelHeight)if pixelWidth != null && pixelHeight != null => (pixelWidth.toInt,pixelHeight.toInt)
      } match {
        case Some((width,height)) =>
          val desiredRatio = width.toFloat/height

          val rawImage = ImageIO.read(file.file)
	        if(rawImage == null){
		        error("File does not exist! This shouldn't happen. ("+file.file.getCanonicalPath+")",file)
	        }
          val currentRatio = rawImage.getWidth.toFloat/rawImage.getHeight
          if(!MathUtils.isEqual(desiredRatio,currentRatio)){
            warn(s"Desired ratio and current ratio of ${file.file.getAbsolutePath} differ (Desired: $desiredRatio Current: $currentRatio)")
          }
          if(width > rawImage.getWidth || height > rawImage.getHeight){
            warn(s"Resizing of ${file.file.getAbsolutePath} would lead to upsampling, skipping.")
          }else{
            val resizedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB)
            val g = resizedImage.createGraphics()
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC)
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
            g.drawImage(rawImage,0,0,width,height,null)

            val outputFile = createTempFile(file)
            ImageIO.write(resizedImage,"PNG",outputFile)
            g.dispose()
            file.file = outputFile
            log("Image resized.",file)
          }
        case None =>
      }
  }
}

object PackTask extends Task {

	/**
	 * Matches: #RRGGBB
	 * Where RR (GG and BB) are hexadecimal digits.
	 * Example:
	 * #FF0056
	 * to capture groups RR GG BB
	 */
	val PreBlendRegex = "#([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])".r

	override val process: TaskFunction = {
    case packDirectory:ResourceDirectory if packDirectory.flags.contains("pack") =>
      val settings = new TexturePacker.Settings()
      settings.limitMemory = false
      settings.useIndexes = false
      settings.filterMag = com.badlogic.gdx.graphics.Texture.TextureFilter.Linear
      settings.filterMin = settings.filterMag
      settings.pot = true //Seems that it is still better for performance and whatnot
      settings.maxWidth = 2048
      settings.maxHeight = settings.maxWidth
      settings.duplicatePadding = true
      settings.bleed = true
      settings.stripWhitespaceX = true
      settings.stripWhitespaceY = true
      settings.alphaThreshold = 0
      packDirectory.files.find(file => file.name == "pack" && file.extension == "json") match {
        case Some(packFile) =>
          Task.json.readFields(settings,Task.jsonReader.parse(new FileReader(packFile.file)))
          packDirectory.removeChild(packFile)
          log("Json packer settings loaded.",packFile.file.getAbsolutePath)
        case None =>
      }
      val packer = new TexturePacker(settings)
      for (image <- packDirectory.files if image.isImage) {
        val bufferedImage = ImageIO.read(image.file)
        if(bufferedImage == null){
          error("Image could not be loaded.",image)
        }else{
          packer.addImage(bufferedImage,image.name + (if(image.flags.contains("9")) ".9" else ""))
          log(s"Image added to pack.",image)
        }
        packDirectory.removeChild(image)
      }

      val atlasName = packDirectory.name
      val outputFolder = createTempDirectory()
      packer.pack(outputFolder, atlasName)

	    val preBlendColor = packDirectory.flags collectFirst {
		    case PreBlendRegex(r,g,b) =>
			    log("Blending color for atlas set.",atlasName)
			    new Color(Integer.parseInt(r,16),Integer.parseInt(g,16),Integer.parseInt(b,16))
	    }

      for(outputJavaFile <- outputFolder.listFiles()){
	      if(Files.getFileExtension(outputJavaFile.getName).equalsIgnoreCase("png")){
		      for(color <- preBlendColor){
			      HelperTasks.preBlendImage(outputJavaFile,color)
		      }
	      }
        packDirectory.parent.addChild(new ResourceFile(outputJavaFile,packDirectory.parent))
      }
      FlattenTask.flatten(packDirectory)
      log("Image atlas packed and directory has been flattened.",atlasName,packDirectory)
  }
}

object CreateFontsTask extends Task {

  val PaddingRegex = "p(t|b|l|r|)(\\d+)".r
  /* Examples:
   * pt56     => Padding left 56
   * p89      => Padding everywhere 89
   */
  val SizeRegex = "(\\d+)".r
  /*
  Example:
  14          => Size 14
   */
  val GlyphRangeRegex = "(\\d+)-(\\d+)".r
  /* Example:
     56-67     => Glyphs 56 to 67 should be added
   */
  val GlyphFileRegex = "from (\\w+)".r
  /* Example:
      from:glyphs   => Will load all glyphs in file "./glyphs". In UTF-8!
   */

	/** Matches bg#RRGGBBAA colors for background. Default is Transparent. */
	val BGRegex = "bg#([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])".r
	/** Matches bg#RRGGBBAA colors for foreground (color of font). Default is White. */
	val FGRegex = "fg#([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])([0-9A-Fa-f][0-9A-Fa-f])".r

  override val process: TaskFunction = {
    case fontFile:ResourceFile if fontFile.isFont =>
      val params = fontFile.flags

      var size:Int = -1
      var paddingLeft = 0
      var paddingRight = 0
      var paddingTop = 0
      var paddingBottom = 0

      fontFile.flags foreach {
        case PaddingRegex(side,amountString) =>
          val amount = amountString.toInt
          side match {
            case "l" => paddingLeft = amount
            case "r" => paddingRight = amount
            case "t" => paddingTop = amount
            case "b" => paddingBottom = amount
            case "" =>
              paddingLeft = amount
              paddingRight = amount
              paddingTop = amount
              paddingBottom = amount
          }
        case SizeRegex(sizeString) =>
          size = sizeString.toInt
        case _ =>
      }

      if(size < 0){
        error("Define size of font. (example.14.ttf)",fontFile)
      }else if(size == 0){
        error("Size must be bigger than 0.",fontFile)
      }else{
        val font = Font.createFont(Font.TRUETYPE_FONT,fontFile.file)

        val hieroSettings = new HieroSettings()
        hieroSettings.setBold(params.contains("b"))
        hieroSettings.setItalic(params.contains("i"))
        hieroSettings.setFontSize(size)
        hieroSettings.setFontName(font.getFontName)
        hieroSettings.setPaddingTop(paddingTop)
        hieroSettings.setPaddingBottom(paddingBottom)
        hieroSettings.setPaddingLeft(paddingLeft)
        hieroSettings.setPaddingRight(paddingRight)

        if(params.contains("native")){
          hieroSettings.setNativeRendering(true)
        }else{
          hieroSettings.setNativeRendering(false)
        }

        val effects = hieroSettings.getEffects.asInstanceOf[java.util.List[Effect]]
        effects.add(new ColorEffect())

        val unicode = new UnicodeFont(fontFile.file.getAbsolutePath,hieroSettings)

        var glyphsAdded = false
        if(params.contains("ascii")){
          unicode.addAsciiGlyphs()
          glyphsAdded = true
        }
        if(params.contains("nehe")){
          unicode.addNeheGlyphs()
          glyphsAdded = true
        }

        for(p <- params){
          p match {
            case GlyphRangeRegex(start,end) =>
              val from = start.toInt
              val to = end.toInt
              unicode.addGlyphs(from,to)
              glyphsAdded = true
              log(s"Added glyphs from $from to $to.")
            case GlyphFileRegex(file) =>
              fontFile.parent.getChildFile(file) match {
                case Some(childFile) =>
                  val reader = Files.newReader(childFile.file,Charsets.UTF_8)
                  var line = reader.readLine()
                  while(line != null){
                    unicode.addGlyphs(line)
                    line = reader.readLine()
                  }
                  reader.close()
                  fontFile.parent.removeChild(childFile)
                  glyphsAdded = true
                  log("Added glyphs from file.",fontFile)
                case None =>
                  warn("File to load glyphs from not found.",fontFile.parent,file)
              }
            case _ =>
          }
        }

        if(!glyphsAdded){//Default
          unicode.addAsciiGlyphs()
          log("No glyphs specified. Thus adding ASCII glyphs.")
        }
        unicode.setGlyphPageWidth(2048)//Should be enough
        unicode.setGlyphPageHeight(2048)

        val hieroUtil = new BMFontUtil(unicode)

        val outputFolder = createTempDirectory()
        val outputFile = new File(outputFolder,fontFile.name)

        hieroUtil.save(outputFile)

        val pageCount = unicode.getGlyphPages.size()
        if(pageCount == 0){
          warn("Font didn't render on any pages.",fontFile)
        }else if(pageCount > 1){
          warn("Font did render on more than one page. This may case problems when loading for UI skin.",pageCount,fontFile)
        }
        log("Font created.",fontFile)
        fontFile.parent.removeChild(fontFile)

	      val bgColor = params collectFirst {
		      case BGRegex(r,g,b,a) =>
			      log("Background color for font set.",fontFile)
			      new Color(Integer.parseInt(r,16),Integer.parseInt(g,16),Integer.parseInt(b,16),Integer.parseInt(a,16))
	      }
	      val fgColor = params collectFirst {
		      case FGRegex(r,g,b,a) =>
			      log("Foreground color for font set.",fontFile)
			      new Color(Integer.parseInt(r,16),Integer.parseInt(g,16),Integer.parseInt(b,16),Integer.parseInt(a,16))
	      }

        for(generatedJavaFile <- outputFolder.listFiles()){
	        if(Files.getFileExtension(generatedJavaFile.getName).equalsIgnoreCase("png")){
		        HelperTasks.clampImage(generatedJavaFile)
		        for(foregroundColor <- fgColor){
			        HelperTasks.multiplyImage(generatedJavaFile,foregroundColor)
		        }
		        for(backgroundColor <- bgColor){
			        HelperTasks.preBlendImage(generatedJavaFile,backgroundColor)
		        }
	        }
          val f = fontFile.parent.addChild(generatedJavaFile)
          log("Font file added.",f)
        }
      }
  }
}

object RemoveEmptyDirectoriesTask extends Task {
  override val process: TaskFunction = {
    case emptyDirectory:ResourceDirectory if !emptyDirectory.hasChildren
      && !emptyDirectory.flags.contains("retain") =>
      log("Empty directory removed.",emptyDirectory)
      emptyDirectory.parent.removeChild(emptyDirectory)
  }
}

object ConvertModelsTask extends Task {

  import scala.collection.convert.wrapAll._

  private val MtlLibRegex = "mtllib (\\w(?:\\w|/)+\\.mtl)".r
  private val TextureRegex = "map_Kd (\\w(?:\\w|/)+\\.(?:png|jpg|jpeg))".r

  private def findDependentFiles(file:ResourceFile,regex:Regex):Iterable[ResourceFile] = {
    Files.readLines(file.file,Charsets.UTF_8).collect[Option[ResourceFile],Iterable[Option[ResourceFile]]]{
      case regex(mtlFileName) =>
        val parts = mtlFileName.split('/')
        var directory = file.parent
        var continue = true
        for(subdirectory <- parts.dropRight(1) if continue){
          directory.getChildDirectory(subdirectory) match {
            case Some(newDirectory) =>
              directory = newDirectory
            case None =>
              error("File references non-existing file.",file,mtlFileName)
              continue = false
          }
        }
        if(continue){
          directory.getChildFile(parts.last) match {
            case Some(mtlFile) =>
              Some(mtlFile)
            case None =>
              error("File references non-existing file.",file,mtlFileName)
              None
          }
        }else{
          None
        }
    }.flatten
  }

  private def removeObjFile(file:ResourceFile){
    file.removeFromParent()
    for(mtlFile <- findDependentFiles(file,MtlLibRegex)){
      mtlFile.parent.removeChild(mtlFile)
      for(textureFile <- findDependentFiles(mtlFile,TextureRegex)){
        textureFile.parent.removeChild(textureFile)
      }
    }
  }

  private def copyObjAndDepsWithoutSpaces(file:ResourceFile){
    val temp = createTempDirectory()
    val objectFile = new File(temp,"object.obj")
    Files.copy(file.file,objectFile)
    file.file = objectFile

    for(mtlFile <- findDependentFiles(file,MtlLibRegex)){
      val mtlFileFile = new File(temp,mtlFile.simpleName)
      Files.copy(mtlFile.file,mtlFileFile)
      mtlFile.file = mtlFileFile
      for(textureFile <- findDependentFiles(mtlFile,TextureRegex)){
        val textureFileFile = new File(temp,textureFile.simpleName)
        Files.copy(textureFile.file,textureFileFile)
        textureFile.file = textureFileFile
      }
    }
  }

  private val OptionsRegex = "options ?((?:\\w| |-)+)".r

  private val ConversionOptionsRegex = "to (fbx|g3dj|g3db)".r

  override val process: TaskFunction = {
    case modelFile:ResourceFile if modelFile.extension == "obj" || modelFile.extension == "fbx" =>
      val isObj = modelFile.extension == "obj"

      val options = modelFile.flags.collectFirst{
        case OptionsRegex(opts) => opts.trim
      }.getOrElse("")

      val convertTo = modelFile.flags.collectFirst{
        case ConversionOptionsRegex(format) =>
          format
      }.getOrElse("g3db")

      if(isObj)copyObjAndDepsWithoutSpaces(modelFile)

      val inputFilePath = modelFile.file.getCanonicalPath

      val outputFilePostfix = "."+convertTo
      val outputFile = new File(createTempDirectory(),modelFile.name+outputFilePostfix)
      val outputFilePath = outputFile.getCanonicalPath

      val postCommandOptions = s" -o ${convertTo.toUpperCase} $options $inputFilePath $outputFilePath"

      log("Converting "+modelFile.extension+" file.",modelFile,postCommandOptions,options,modelFile.flags.addString(new StringBuilder, "|").toString())

      if(isObj)removeObjFile(modelFile)
      else modelFile.removeFromParent()

      ExecutablesDirectory

      LWJGLUtil.getPlatform match {
        case LWJGLUtil.PLATFORM_MACOSX =>
          executeCommand(postCommandOptions)
        case LWJGLUtil.PLATFORM_WINDOWS =>
          error("Fbx-conv on Windows not yet supported.")
        case LWJGLUtil.PLATFORM_LINUX =>
          error("Fbx-conv on Linux not yet supported.")
        case unk =>
          error("Unknown platform reported by LWJGL",unk)
      }
      modelFile.parent.addChild(outputFile)
  }

  private def copyResourceToFolder(destination:File,resource:String){
    Files.createParentDirs(destination)
    val in = PackingOperation.getClass.getClassLoader.getResourceAsStream(resource)
    val out = new FileOutputStream(destination)
    ByteStreams.copy(in,out)
    out.flush()
    out.close()
    in.close()
    destination.setExecutable(true,false)
  }

  private var executable:File = null

  private lazy val ExecutablesDirectory:File = {
    val result = createTempDirectory()
    LWJGLUtil.getPlatform match {
      case LWJGLUtil.PLATFORM_MACOSX =>
        executable = new File(result,"fbx-conv-mac")
        copyResourceToFolder(executable,"fbxconv/osx/fbx-conv-mac")
        copyResourceToFolder(new File(result,"libfbxsdk.dylib"),"fbxconv/osx/libfbxsdk.dylib")
      case LWJGLUtil.PLATFORM_WINDOWS =>
        executable = new File(result,"fbx-conv-win32.exe")
        copyResourceToFolder(executable,"fbxconv/windows/fbx-conv-win32.exe")
      case LWJGLUtil.PLATFORM_LINUX =>
        executable = new File(result,"fbx-conv-lin64")
        copyResourceToFolder(executable,"fbxconv/linux/fbx-conv-lin64")
        copyResourceToFolder(new File(result,"libfbxsdk.so"),"fbxconv/linux/libfbxsdk.so")
      case _ =>
    }
    result
  }

  import scala.sys.process.Process
  private def executeCommand(postCommand:String){
    val command = executable.getCanonicalPath+postCommand
    log("Executing external command.",command)
    val processResult = Process(command).!
    if(processResult == 0){
      log("External command terminated normally.")
    }else{
      warn("External command terminated with unusual code.",processResult)
    }
  }
}
