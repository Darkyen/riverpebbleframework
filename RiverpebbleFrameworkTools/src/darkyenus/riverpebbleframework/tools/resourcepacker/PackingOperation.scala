package darkyenus.riverpebbleframework.tools.resourcepacker

import java.io.File

import com.badlogic.gdx.{Gdx, ApplicationListener}
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.{GL20, Color}

import scala.util.Try

/**
 * Private property.
 * User: Darkyen
 * Date: 18/07/14
 * Time: 12:41
 */
private class PackingOperation private (from:File,to:File) {
  val root = PackingOperation.createTree(from)

  if(root.flags.nonEmpty)warn("Flags of root will not be processed.")

  private def prepareOutputDirectory(){
    clearFolder(to)
    if(!to.exists() && !to.mkdirs()){
      warn("Directory could not be created. Assuming it's fine.",to)
    }
  }

  private def applyTasks(){
    for(task <- Task.All){
      root.applyTask(task)
    }
  }

  private def copyResults(){
    root.copyYourself(to,root = true)
  }

  def process(options:Map[Symbol,String]){
    prepareOutputDirectory()
    Task.prepare(options)
    applyTasks()
    copyResults()
    Task.end()
  }
}

object PackingOperation {

  private def createTree(root:File):ResourceDirectory = {
    if(!root.isDirectory)sys.error(s"${root.getAbsolutePath} is not a directory.")
    val result = new ResourceDirectory(root,null)
    result.parent = result
    result.create()
    result
  }

  def apply(from:File,to:File,options:Map[Symbol,String]){
    if(from.exists()){
      log("Starting packing operation. (From,To,Options)",from.getCanonicalPath,to.getCanonicalPath,options)
      new PackingOperation(from,to).process(options)
      log("Packing operation is done.")
    }else{
      error("Resource folder does not exist.",from.getCanonicalPath)
    }
  }

  def process(in:File,out:File,options:Map[Symbol,String],waitForCompletion:Boolean = true,forceExit:Boolean = true){
    val config = new LwjglApplicationConfiguration
    config.backgroundFPS = 1
    config.foregroundFPS = 1
    config.title = "ResourcePacker2"
    config.allowSoftwareMode = true
    LwjglApplicationConfiguration.disableAudio = true
    config.forceExit = forceExit
    config.resizable = false
    config.width = 200
    config.height = 1
    config.r = 1
    config.g = 1
    config.b = 1
    config.a = 1
    config.initialBackgroundColor = new Color(0.5f, 0.5f, 0.5f, 1f)

    val app = new LwjglApplication(new ApplicationListener {
      override def resize(width: Int, height: Int) {}

      override def dispose() {}

      override def pause() {}

      override def render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
      }

      override def resume() {}

      override def create() {
        PackingOperation(in,out,options)
        Gdx.app.exit()
      }
    }, config)
    if(waitForCompletion){
      try{
        val loopThreadField = app.getClass.getDeclaredField("mainLoopThread")
        loopThreadField.setAccessible(true)
        val loopThread = loopThreadField.get(app).asInstanceOf[Thread]
        loopThread.join()
      }catch {
        case e:Exception =>
          println("Waiting for main loop to stop failed. Incoming stack trace.")
          e.printStackTrace(Console.out)
      }
    }
  }
}