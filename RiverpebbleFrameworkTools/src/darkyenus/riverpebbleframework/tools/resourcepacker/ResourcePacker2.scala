package darkyenus.riverpebbleframework.tools.resourcepacker

import java.io.File

/**
 * Private property.
 * User: Darkyen
 * Date: 16/07/14
 * Time: 17:56
 */
object ResourcePacker2 extends App {

  assume(args.length >= 2,"Usage: <InputFolderStructure> <OutputFolder> [OptionalArgKey[=Value]]*")

  val resourceFolder = new File(args(0))
  assume(resourceFolder.isDirectory,s"Please supply an existing input folder. (${resourceFolder.getAbsolutePath} is not valid)")
  val outputFolder = new File(args(1))

  val options:Map[Symbol,String] = args.drop(2).map(opt => opt.span(_ != '=')).map(pair => (Symbol(pair._1),pair._2.stripPrefix("="))).toMap

  PackingOperation.process(resourceFolder,outputFolder,options)
}
