package darkyenus.riverpebbleframework.tools

import java.io.File

/**
 * Private property.
 * User: Darkyen
 * Date: 16/07/14
 * Time: 20:55
 */
package object resourcepacker {

  final val LogLevelINFO = 1
  final val LogLevelWARN = 2
  final val LogLevelERR = 3

  var LogLevel = 0

  private final val LogPrefix = Console.BLUE+"INFO: "+Console.RESET
  def log(message:String,meta:Any*){
    if(LogLevel <= LogLevelINFO){
      println(format(LogPrefix,message,meta))
    }
  }

  private final val WarnPrefix = Console.YELLOW+"WARNING: "+Console.RESET
  def warn(message:String,meta:Any*){
    if(LogLevel <= LogLevelWARN){
      println(format(WarnPrefix,message,meta))
    }
  }

  private final val ErrorPrefix = Console.RED+"ERROR: "+Console.RESET
  def error(message:String,meta:Any*){
    if(LogLevel <= LogLevelERR){
      println(format(ErrorPrefix,message,meta))
    }
  }

  private val builder = new StringBuilder

  private def format(prefix:String,message:String,meta:Seq[Any]):String = {
    builder.append(prefix)
    builder.append(message)
    if(meta.nonEmpty){
      builder.append(" (")
      meta.addString(builder,", ")
      builder.append(')')
    }
    val result = builder.toString()
    builder.clear()
    result
  }

  //---------------------------------------------------------------------------------------------------------

  def clearFolder(dir:File,deleteThis:Boolean = false){
    if(dir.isDirectory){
      for(file <- dir.listFiles()){
        if(file.isFile){
          if(!file.delete()){
            warn(s"File ${file.getPath} not deleted.")
          }
        }else if(file.isDirectory){
          clearFolder(file,deleteThis = true)
        }
      }
      if(deleteThis && !dir.delete()){
        warn(s"Directory ${dir.getPath} not deleted.")
      }
    }else if(dir.isFile){
      warn(s"Directory ${dir.getPath} is actually a file.")
    }
  }
}
