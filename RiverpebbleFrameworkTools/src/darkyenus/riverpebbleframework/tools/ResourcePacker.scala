package darkyenus.riverpebbleframework.tools

import java.awt.image.BufferedImage
import java.awt.{Desktop, Font, RenderingHints}
import java.io.{File, FileReader}
import javax.imageio.ImageIO

import com.badlogic.gdx.backends.lwjgl._
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.tools.hiero.BMFontUtil
import com.badlogic.gdx.tools.hiero.unicodefont.effects.{ColorEffect, Effect}
import com.badlogic.gdx.tools.hiero.unicodefont.{HieroSettings, UnicodeFont}
import com.badlogic.gdx.tools.texturepacker.TexturePacker
import com.badlogic.gdx.utils.{Json, JsonReader}
import com.badlogic.gdx.{ApplicationListener, Gdx}
import com.google.common.io.Files
import org.lwjgl.opengl.Display

import scala.util.Try

/**
 * Private property.
 * User: Darkyen
 * Date: 19/05/14
 * Time: 19:51
 *
 * @deprecated Use ResourcePacker2 instead, this is buggy mess
 */
@deprecated(message = "Use ResourcePacker2 instead, this is buggy mess", since = "Ever")
object ResourcePacker extends App {
  val startTime = System.currentTimeMillis()

  assume(args.length >= 2,"Usage: <InputFolderStructure> <OutputFolder> [OptionalArgKey=IntValue]")

  val resourceFolder = new File(args(0))
  assume(resourceFolder.isDirectory,s"Please supply an existing input folder. (${resourceFolder.getAbsolutePath} is not valid)")
  val outputFolder = new File(args(1))
  val debug = args.contains("DEBUG")

  val json = new Json()
  //Clear output so no overwriting
  def clearFolder(dir:File){
    if(dir.isDirectory){
      for(file <- dir.listFiles()){
        if(file.isFile){
          if(!file.delete()){
            println(s"WARNING: File ${file.getPath} not deleted.")
          }
        }else if(file.isDirectory){
          clearFolder(file)
        }
      }
      if(!dir.delete()){
        println(s"WARNING: Directory ${dir.getPath} not deleted.")
      }
    }else if(dir.isFile){
      println(s"WARNING: Directory ${dir.getPath} is actually a file.")
    }
  }
  clearFolder(outputFolder)
  val options:Map[Symbol,String] = args.drop(2).map(opt => opt.span(_ != '=')).map(pair => (Symbol(pair._1),pair._2.tail)).toMap

  def walkFiles(dir:File,filter:(String)=>Boolean,action:(String,File)=>Unit,previousPath:String = ""){
    assume(dir.exists() && dir.isDirectory, s"Please make sure that ${dir.getAbsolutePath} exists and is a directory.")
    for(file <- dir.listFiles()){
      if(!file.isHidden){
        if(file.isDirectory){
          walkFiles(file,filter,action,previousPath + file.getName + "/")
        }else if(file.isFile){
          if(filter(file.getName.toLowerCase)){
            action(previousPath,file)
          }
        }
      }
    }
  }

  def walkFolders(dir:File,filter:Array[File] => Boolean,action:(String,File)=>Unit,previousPath:String = ""){
    assume(dir.exists() && dir.isDirectory, s"Please make sure that ${dir.getAbsolutePath} exists and is a directory.")
    if(filter(dir.listFiles())){
      action(previousPath,dir)
    }
    for(file <- dir.listFiles()){
      if(!file.isHidden && file.isDirectory){
        val newPath = previousPath + file.getName + "/"
        walkFolders(file,filter,action,newPath)
      }
    }
  }

  def isImage(name:String):Boolean = {
    val lowerCase = name.toLowerCase
    lowerCase.endsWith(".png") || lowerCase.endsWith(".jpg") || lowerCase.endsWith(".jpeg")
  }

  def isImage(name:File):Boolean = {
    isImage(name.getName)
  }

  def isFont(name:String):Boolean = {
    name.toLowerCase.endsWith(".ttf")
  }

  {
    val config = new LwjglApplicationConfiguration
    config.backgroundFPS = 1
    config.foregroundFPS = 1
    config.title = "ResourcePacker"
    config.allowSoftwareMode = true
    LwjglApplicationConfiguration.disableAudio = true
    config.forceExit = true
    config.resizable = false
    config.width = 200
    config.height = 1
    config.r = 1;config.g = 1;config.b = 1;config.a = 1
    config.initialBackgroundColor = new Color(0.5f,0.5f,0.5f,1f)

    new LwjglApplication(new ApplicationListener {
      override def resize(width: Int, height: Int){}

      override def dispose(){
        Try(Thread.sleep(500))
        //Let window close with style.
      }

      override def pause(){}

      override def render(){}

      override def resume(){}

      override def create(){

        val tempFolder = Files.createTempDir()

        var statFonts = 0
        var statImages = 0
        var statAtlases = 0
        var statOther = 0

        //Process fonts
        {
          walkFiles(resourceFolder,isFont, (hierarchy,file) => {
            val filename = Files.getNameWithoutExtension(file.getName)
            val parts = filename.split('.')
            val resultName = parts.head
            val params = parts.tail
            val size = Try(params.find(_.forall(_.isDigit)).get.toInt).getOrElse(sys.error("Specify size of font \""+file.getPath+"\""))
            val padding = params.find(s => {s.head == 'p' && s.tail.forall(_.isDigit)}).fold(0)(_.toInt)
            val paddingTop = params.find(s => {s.head == 'p' && s.tail.head == 't' && s.tail.tail.forall(_.isDigit)}).fold(padding)(_.toInt)
            val paddingBottom = params.find(s => {s.head == 'p' && s.tail.head == 'b' && s.tail.tail.forall(_.isDigit)}).fold(padding)(_.toInt)
            val paddingLeft = params.find(s => {s.head == 'p' && s.tail.head == 'l' && s.tail.tail.forall(_.isDigit)}).fold(padding)(_.toInt)
            val paddingRight = params.find(s => {s.head == 'p' && s.tail.head == 'r' && s.tail.tail.forall(_.isDigit)}).fold(padding)(_.toInt)

            val font = Font.createFont(Font.TRUETYPE_FONT,file)

            val hieroSettings = new HieroSettings()
            hieroSettings.setBold(params.contains((i:String) => {i == "b"}))
            hieroSettings.setItalic(params.contains((i:String) => {i == "i"}))
            hieroSettings.setFontSize(size)
            hieroSettings.setFontName(font.getFontName)
            hieroSettings.setPaddingTop(paddingTop)
            hieroSettings.setPaddingBottom(paddingBottom)
            hieroSettings.setPaddingLeft(paddingLeft)
            hieroSettings.setPaddingRight(paddingRight)

            val effects = hieroSettings.getEffects.asInstanceOf[java.util.List[Effect]]
            effects.add(new ColorEffect())

            val unicode = new UnicodeFont(file.getAbsolutePath,hieroSettings)

            var glyphsAdded = false
            if(params.contains("ascii")){
              unicode.addAsciiGlyphs()
              glyphsAdded = true
            }
            if(params.contains("nehe")){
              unicode.addNeheGlyphs()
              glyphsAdded = true
            }
            val glyphRegex = "\\d+-\\d+".r
            params.collectFirst({
              case glyphRegex(start,end) =>
                unicode.addGlyphs(start.toInt,end.toInt)
                glyphsAdded = true
            })

            if(!glyphsAdded){//Default
              unicode.addAsciiGlyphs()
            }
            unicode.setGlyphPageWidth(2048)//Should be enough
            unicode.setGlyphPageHeight(2048)

            val hieroUtil = new BMFontUtil(unicode)
            val resultFile = new File(tempFolder,hierarchy+resultName)
            Files.createParentDirs(resultFile)
            hieroUtil.save(resultFile)

            val pageCount = unicode.getGlyphPages.size()
            if(pageCount == 0){
              println("WARNING: Font \""+resultName+"\" didn't render on any pages.")
            }else if(pageCount > 1){
              println("WARNING: Font \""+resultName+"\" rendered on "+pageCount+" pages. This may case problems when loading for UI skin.")
            }
            statFonts += 1
          })
        }

        {//Atlases
          {//Resize and copy images to temp

            lazy val tileSize = options('TileSize).toInt
            def tileFraction(input:String):Int = {
              (input.replace(',','.').toFloat * tileSize).round
            }

            walkFiles(resourceFolder,isImage,(hierarchy,file)=>{
              val fileNamePartsRaw = file.getName.split('.')
              val fileExtension = fileNamePartsRaw.takeRight(1).head
              val fileNameParts = fileNamePartsRaw.dropRight(1)
              val resultName = fileNameParts.head + (if(fileNameParts.contains("9")) ".9" else "")

              val TileSizePattern = """w(\d+(?:,\d+)?)h(\d+(?:,\d+)?)""".r
              val PixelSizePattern = """(\d+)x(\d+)""".r

              val resize:Option[(Int,Int)] = fileNameParts.tail.collectFirst {
                case TileSizePattern(tileWidth,tileHeight) => (tileFraction(tileWidth),tileFraction(tileHeight))
                case PixelSizePattern(pixelWidth,pixelHeight) => (pixelWidth.toInt,pixelHeight.toInt)
              }

              resize match {
                case Some((width:Int,height:Int)) =>
                  val desiredRatio = width.toFloat/height

                  val rawImage = ImageIO.read(file)
                  val currentRatio = rawImage.getWidth.toFloat/rawImage.getHeight
                  if(!MathUtils.isEqual(desiredRatio,currentRatio)){
                    println(s"WARNING: Desired ratio and current ratio of ${hierarchy+file.getName} differ (Desired: $desiredRatio Current: $currentRatio)")
                  }
                  if(width > rawImage.getWidth || height > rawImage.getHeight){
                    println(s"WARNING: Resizing of ${hierarchy+file.getName} would lead to upsampling, skipping.")
                  }else{
                    val resizedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB)
                    val g = resizedImage.createGraphics()
                    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC)
                    g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)
                    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
                    g.drawImage(rawImage,0,0,width,height,null)

                    val outputFile = new File(new File(tempFolder,hierarchy),resultName+".png")
                    Files.createParentDirs(outputFile)
                    ImageIO.write(resizedImage,"PNG",outputFile)
                    g.dispose()
                  }
                case None =>
                  val outputFile = new File(new File(tempFolder,hierarchy),resultName+"."+fileExtension)
                  Files.createParentDirs(outputFile)
                  Files.copy(file,outputFile)
              }
            })
          }
          {//Pack atlases
            walkFolders(tempFolder, _.exists(isImage), (hierarchy, folder) => {
              //Pack only those that really contain some stuff
              val atlasName = if (hierarchy.isEmpty) "resources" else folder.getName
              val settings = new TexturePacker.Settings()
              settings.limitMemory = false
              settings.useIndexes = false
              settings.filterMag = com.badlogic.gdx.graphics.Texture.TextureFilter.Linear
              settings.filterMin = settings.filterMag
              settings.pot = true //Seems that it is still better for performance and whatnot
              settings.maxWidth = 2048
              settings.maxHeight = settings.maxWidth
              settings.duplicatePadding = true
              settings.bleed = true
              settings.stripWhitespaceX = true
              settings.stripWhitespaceY = true
              settings.alphaThreshold = 0
              val packSettginsFile = new File(new File(resourceFolder,hierarchy),"pack.json")
              if(packSettginsFile.exists() && packSettginsFile.isFile && packSettginsFile.canRead){
                json.readFields(settings, new JsonReader().parse(new FileReader(packSettginsFile)))
                println("Json settings loaded ("+packSettginsFile.getPath+")")
              }
              val packer = new TexturePacker(folder, settings)
              for (image <- folder.listFiles() if image.isFile && isImage(image)) {
                packer.addImage(image)
                println(s"Added to pack: ${image.getName}")
                statImages += 1
              }
              packer.pack(outputFolder, atlasName)
              println(s"Packed $hierarchy to $atlasName")
              statAtlases += 1
            })
          }
        }

        {//Copy remaining assets in temporary folder (fnt...)
          walkFiles(tempFolder,!isImage(_),(hierarchy,file)=>{
            if(file.getName != "pack.json"){
              val outputFile = new File(outputFolder,file.getName)
              println(s"Copying from temp ${file.getName}")
              Files.copy(file,outputFile)
              statOther += 1
            }
          })
        }
        {//Copy remaining assets (skin,sounds...)

          def toFlagName(folderName:String):List[String] = {
            val parts = folderName.split('.')
            parts.head :: parts.tail.map(_.toLowerCase).toList
          }

          def hasRetainFlag(folder:List[String]):Boolean = {
            folder.tail.contains("retain")
          }

          def getHierarchy(folder:File):List[String] = {
            folder match {
              case `resourceFolder` =>
                Nil
              case normalParent =>
                normalParent.getName :: getHierarchy(folder.getParentFile)
            }
          }

          def getFileOutputFolder(file:File):File = {
            val hierarchy = getHierarchy(file.getParentFile).map(toFlagName)//List(topmost,...,folder-in-resourceFolder)
            if(hierarchy.forall(!hasRetainFlag(_))){
              //There is no retain flag whatsoever
              outputFolder
            }else{
              var terminate = false
              val retainedHierarchy = hierarchy.takeWhile(f => {terminate = hasRetainFlag(f);terminate})
              retainedHierarchy.foldRight(outputFolder)((flagFolder,folder) => new File(folder,flagFolder.head))
            }
          }

          walkFiles(resourceFolder,f => !isImage(f) && !isFont(f),(hierarchy,file)=>{
            if(file.getName != "pack.json"){
              val fileOutputFolder = getFileOutputFolder(file)
              val outputFile = new File(fileOutputFolder,file.getName)
              Files.createParentDirs(outputFile)
              println(s"Copying from assets ${file.getName}")
              Files.copy(file,outputFile)
              statOther += 1
            }
          })
        }

        //Cleanup
        clearFolder(tempFolder)

        //DEBUG
        if(debug) {
          Desktop.getDesktop.open(outputFolder)
        }

        //End
        println()
        println("Packing completed.")
        val duration = System.currentTimeMillis() - startTime
        println(s"Rasterized $statFonts font${if(statFonts == 1) "" else "s"}, packed $statImages image${if(statImages == 1) "" else "s"} into $statAtlases atlas${if(statAtlases == 1) "" else "es"} and copied $statOther file${if(statOther == 1) "" else "s"} in $duration milliseconds.")
        Display.setTitle("Done")
        Gdx.app.exit()
      }
    },config)
  }
}
